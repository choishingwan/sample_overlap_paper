# Introduction

This repository contain all scripts required to replicate simulations performed in the EraSOR paper. 
To replicate our findings, you will need the followings:


1. Access to UK biobank data

2. Install Nextflow `curl -s https://get.nextflow.io | bash`

3. Have singularity installed ([here](https://sylabs.io/guides/3.6/user-guide/quick_start.html#quick-installation-steps)) 

4. Download the singularity image with all the required software installed - `singularity pull library://choishingwan/erasor/erasor:latest`

5. 1000 Genome phase 3 genotype file (bed format). This is required for LD score calculation.  

6. 1000 Genome recombination map. This is required for HapGen simulation. Can be downloaded [here](http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/working/20130507_omni_recombination_rates/) 

7. 1000 Genome phase 3 legend and hap file. Required for HapGen simulation. Can be downloaded [here](https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3.html)

8. GLGC summary statistic files. Required for the real data LDL analyses. Can be downloaded from [here](https://csg.sph.umich.edu/willer/public/glgc-lipids2021/). Specifically, you will need the LDL_INV_EUR_HRC_1KGP3_others_ALL.meta.singlevar.results.gz, without_UKB_LDL_INV_EUR_HRC_1KGP3_others_ALL.meta.singlevar.results.gz, without_UKB_meta-analysis_AFR_EAS_EUR_HIS_SAS_LDL_INV_ALL_with_N_1.gz and with_BF_meta-analysis_AFR_EAS_EUR_HIS_SAS_LDL_INV_ALL_with_N_1.gz

You can then find the following scripts in the `script` folder

- `1_prepare_ldscore.nf` : For generating the LD score. Require the 1000 genome phase 3 genotype files

- `2_simulation.nf` : The main simulation script. Perform simulation using the EUR UKB samples. 

- `3_fst_simulation.nf` : HapGen2 simulations script. Require the recombination map and the 10000 genome genotypes. 

- `4_ukb_fst_simulation.nf` : Re-run main simulation but with non-European samples. Only focus on quantitative traits to reduce computation burden. 

- `5_ukb_real_data.nf`: Pipeline to perform the real data analyses in UKB. 

- `6_ukb_ldl.nf`: This is the pipeline responsible for performing the GLGC analyses

- `7_generate_figure.R` : This is the Rscript used to generate the figures in the paper. It requires the output from script 1 to 5. Main figures are named accordingly (Figure 1-3), whereas supplementary figures were not numbered. 

> Note: Results from script 3 was no longer included in the paper as the simulated genotype from HapGen2 does not have the appropriate Fst.
