#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.3'

timestamp='2021-12-24'
if(params.version) {
    System.out.println("")
    System.out.println("Run sample overlap simulation with African and European samples- Version: $version ($timestamp)")
    exit 1
}

params.seed = 1234
params.develop = false
if(params.help){
    System.out.println("")
    System.out.println("Run sample overlap simulation - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 4_ukb_fst_simulation.nf [options]")
    System.out.println("Mandatory arguments:")
    Sysmte.out.println("    --sql         UK Biobank SQLite database")
    System.out.println("    --score       Folder containing LD Scores. Should have")
    System.out.println("                  format of baseline-chr and weight-chr")
    System.out.println("    --geno        Genotype file prefix ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --drop        Samples who withdrawn consent")
    System.out.println("    --cov         Covariate file containing the PCs")
    System.out.println("    --greedy      GreedyRelated software for relatedness filtering")
    System.out.println("    --rel         UK Biobank relatedness information")
    System.out.println("    --perm        Total permutation")
    System.out.println("    --seed        Seed for random number generation")
    System.out.println("    --help        Display this help messages")
} 



////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def get_prefix(str){
    if(str.split('-')[0] == "NA"){
        error("\n\n----------\n$str cannot be tokenized\n")
    }else 
        return str.split('-')[0];
}
def gen_file(chr, input){
    return input.replaceAll("#",chr.toString())
}

def get_chr(a, input){
    if(input.contains("#")){
        return a
    }
    else {
        return 0
    }
}
////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   removeElements
            addMeta
            removeMeta
            combine_map  }   from './modules/handle_meta'
include {   calculate_xbeta
            generate_phenotype
            select_base_samples
            select_target_samples
            define_population   }   from './modules/phenotype_simulation'
include {   outliers_aneuploidy_related
            basic_qc
            remove_dropout_and_invalid
            extract_biological_sex
            calculate_stat_for_sex
            generate_high_ld_region
            filter_sex_mismatch
            finalize_data
            relatedness_filtering
            prunning  }   from './modules/quality_control'
include {   run_gwas    }   from './modules/basic_genetic_analyses'
include {   run_erasor
            run_prsice
            update_prsice
            modify_erasor_meta
            combine_files
            combine_files as combine_prsice
            combine_tmp_files
            process_overlap_results  }   from './modules/polygenic_analyses'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////

genotype = Channel
    .fromFilePairs("${params.geno}.{bed,bim,fam}",size:3, flat : true){ file -> file.baseName }
    .ifEmpty { error "No matching plink files" }        
    .map { a -> [fileExists(a[1]), fileExists(a[2]), fileExists(a[3])] } 
qc_fam = Channel.fromPath("${params.fam}")
cov = Channel.fromPath("${params.cov}")
snp = Channel.fromPath("${params.snp}")
sql = Channel.fromPath("${params.sql}")
withdrawn = Channel.fromPath("${params.drop}")
greedy = Channel.fromPath("${params.greedy}")
rel = Channel.fromPath("${params.rel}")

score = Channel.fromPath("${params.score}/*") \
    | map { a -> [ get_prefix(a.baseName)+"-", file(a)]} \
    | groupTuple()

def index = 0
perm_seed = Channel
    .from( 1 .. 100000 )
    .randomSample( params.perm, params.seed )
    .map{ a-> [index++, a] }

workflow{
    // We want to extract some non-European samples for this analysis. Normally, we would 
    // also want to re-calculate the LD score due to the additional non-EUR samples. But then
    // considering the % of sample (~4%), the LD score should not change too much. So let's
    // stick to existing LD score 
    data_preparation()
    
    phenotype_simulation(   
        data_preparation.out.population,
        data_preparation.out.data)
        
    generate_study_samples(
        data_preparation.out.population,
        phenotype_simulation.out
    )
    
    generate_summary_statistics(
        generate_study_samples.out
    )
    polygenic_score_analyses(
        generate_study_samples.out,
        generate_summary_statistics.out
    )
}


workflow data_preparation{
    main:
    // First, we want to extract the non-UK birth origin
    // so that when we do the kmean clustering, we can use
    // this information to inform the possible population without
    // needing to eyeball the data
        cov \
            /* We will use a 4 mean clustering to identify the population */
            | combine(Channel.of(4)) \
            /* 
                k mean has an element of randomness, so we need the seed parameter to 
                ensure what we did is replicable
            */
            | combine(Channel.of(params.seed))\
            /*
                This should output one file that contain the selected populations
            */
            | define_population
        /* 
            Problem with existing UKB QCed sample is that they were done 
            purely on European samples. As a result, all African samples 
            were removed. For us to get post-QC african samples, we will 
            need to repeat part of the UK Biobank QC
        */
        outliers_aneuploidy_related(sql)
        remove_dropout_and_invalid( 
            genotype, 
            outliers_aneuploidy_related.out.outliers, 
            withdrawn)
        // given the list of invalid samples and the populations that we are
        // interested in, perform some basic QC so that we can start working 
        // on Prunning, sex check and relatedness checking
        basic_qc(   
            genotype,
            define_population.out.pop,
            remove_dropout_and_invalid.out.removed, 
            1e-8, 
            0.02, 
            0.01)
        /*
            Will need to generate regions of high LD. This regions were obtained from Joni.
        */
        generate_high_ld_region(    
            basic_qc.out.qc, 
            genotype, 
            "grch37")
        /*
            Can now perform prunning using the generated data. We hard code the window size to 200, window step as 50, r2 as 0.02 and 
            maximum sample size as 10000
            Don't allow customization here as that isn't the main purpose of this code

            Note: this might be slightly problematic as we have included both EUR and AFR samples in the data. Though that should in 
            theory not affect our sex check too much
        */

        prunning(   
            genotype,
            basic_qc.out.qc, 
            generate_high_ld_region.out, 
            200,    // window size
            50,     // window step size
            0.02,   // pruning R2
            10000,  // maximum sample used for R2 calculation
            params.seed)
        /* 
            We can now perform the sex check. Can compare the biological sex against the estimated sex of the sample
        */  
        extract_biological_sex(sql)
        /*
            Perform the genetic sex estimation using PLINK
        */
        calculate_stat_for_sex( 
            genotype,
            basic_qc.out.qc,
            prunning.out)
        /* 
            Given the sex information and the estimate, we can filter out samples that have mismatch sex information
        */
        filter_sex_mismatch(    
            basic_qc.out.qc, 
            calculate_stat_for_sex.out,
            extract_biological_sex.out)
        /*
            Now run the relatedness filtering using the greedy algorithm implemented in greedyRelated
        */
        relatedness_filtering(  greedy,
                                rel,
                                filter_sex_mismatch.out.valid,
                                0.044, 
                                params.seed)
        /*
            At the end, we can extract the valid SNPs and sample
        */
        finalize_data(  genotype,
                        basic_qc.out.qc, 
                        filter_sex_mismatch.out.mismatch,
                        relatedness_filtering.out.removed)
    emit: 
        population = define_population.out.pop
        data = finalize_data.out.qced
}
workflow phenotype_simulation{
    take: pop
    take: qced
    main: 
        /*
            For this analysis, we should have the valid samples based on previous filtering. So we can directly
            skip to the calculate_xbeta part

            We assume there are 10,000 causal variants, with effect size following a normal distribution.
            Genotypes were standardized before calculating the xbeta
        */
        numCausal = Channel.of(10000)
        heritability = Channel.of(0, 0.1, 0.5)
        sigmaS = Channel.of(0, 0.3, 0.9)
        qced \
            | combine(numCausal) \
            | combine(perm_seed) \
            | combine(genotype) \
            | map{ a -> def meta = [:]
                        meta["numCausal"] = a[2]
                        meta["perm"] = a[3]
                        meta["seed"] = a[4]
                        return([
                            meta,
                            a[1],   // qced fam file
                            [],     // stub for relatedness
                            a[5],   // bed
                            a[6],   // bim
                            a[7],   // fam
                            a[0]    // qced SNP
                        ])} \
            | calculate_xbeta \
            /*
                Once we have calculated the xbeta, we can start simulating 
                the phenotype
            */
            | combine(heritability) \
            | combine(sigmaS) \
            | map{ a -> def meta = a[0].clone()
                        meta["herit"] = a[3]
                        meta["sigmaS"] = a[4]
                        // now do conditional return
                        def result = [  meta,   // meta information
                                        a[1],   // related information
                                        a[2]]   // xbeta
                        if(meta["herit"] + meta["sigmaS"] < 1){
                            return(result)
                        }
            } \
            | combine(pop) \
            /*
                Once we have obtained the XBETA, we will simulate data with sigmaS by 
                giving a different level of environmental contribution to EUR and non-EUR samples
                EUR will get sqrt(sigmaS / 2) whereas non-EUR will get -sqrt(sigmaS / 2)

                To test our hypothesis of inflation caused by unmeasured error, we also incorporate a correlation
                structure between the error term in base and target
            */
            | generate_phenotype
    emit: 
        generate_phenotype.out
}


workflow generate_study_samples{
    take: pop
    take: pheno
    main:
        // 1. First, randomly select N samples as base. We don't perform binary or related simulation
        // here because of the lack of sample size. 
        baseSize = Channel.of(250000, 120000)
        pheno \
            | combine(baseSize) \
            | map{ a -> def meta = a[0].clone()
                    meta["baseSize"] =  a[3]
                    def baseData = [    meta,   // meta information
                                        a[1],   // related information
                                        a[2]]   // phenotype
                    // only iterate the base sample size when heritability = 0 or 0.1    
                    if(meta.baseSize == 250000 || meta.herit == 0 || meta.herit == 0.1){
                        return(baseData)
                    }
                } \
            | combine(pop) \
            | select_base_samples

        overlap = Channel.of(0, 0.1, 0.5, 1) 
        targetSize = Channel.of(5000, 10000)
        select_base_samples.out \
            | combine(overlap) \
            | combine(targetSize) \
            | map{ a -> def meta = a[0].clone()
                        meta["overlap"]  = a[4]
                        meta["targetSize"] = a[5]
                        def result = [  meta,   // meta information
                                        a[1],   // related pair
                                        a[2],   // phenotype
                                        a[3]]   // base ID
                        // only alter base size when target size is 5000 and vis versa
                        if( meta.herit == 0 || meta.herit==0.1 || 
                            meta.baseSize == 250000 || 
                            meta.targetSize == 5000)
                                return(result)
                        
                } \
            | unique \
            | combine(pop) \
            | select_target_samples

        /*
            When overlap is 0, expected == target, so we don't want to repeat the analysis for that as it can be expensive
        */
        samples = select_target_samples.out \
            | mix \
            | mix(select_base_samples.out)  \
            | filter{ a -> a[0].sample != "Expected" || a[0].overlap != 0 } 
    emit:
        samples
}


workflow generate_summary_statistics{
    take: samples
    main:
        // 1. Perform GWAS analysis on base and target samples
        samples \
            | combine(Channel.of(["PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,PC11,PC12,PC13,PC14,PC15"]))  \
            | map{ a ->  addMeta(x: a, meta: [cov: a[4]])} \
            | map{ a -> removeElements(x: a, idx: [4])} \
            | combine(genotype) \
            | combine(snp) \
            | combine(cov) \
            | unique \
            | run_gwas
        // 2. Extract the base GWAS
        base = run_gwas.out \
            | filter{ a -> a[0].sample == "Base"}
        target = run_gwas.out \
            | filter{ a -> a[0].sample == "Target"}
        expected = run_gwas.out \
            | filter{ a -> a[0].sample == "Expected"}
        // 3. Extract the ld score to appropriate groups
        baseline = score \
            | filter { a -> (a[0] =~ /baseline/) }
        weight = score \
            | filter { a -> (a[0] =~ /weight/) }
        // 4. Run EraSOR to obtain the adjusted GWAS
    
        combine_map(x: target, 
                    y: base, 
                    by: 0,
                    column: ["perm", "herit","numCausal", "sigmaS", "cov", "baseSize"]) \
            | combine(baseline) \
            | combine(weight) \
            | combine(Channel.of("${params.erasor}")) \
            | run_erasor 
        metaResult = run_erasor.out.info \
            | unique \
            | modify_erasor_meta \
            | map{ a -> a[1]} \
            | collect 
        gwas = run_erasor.out.gwas \
            | mix(base) \
            | mix(expected)
        //combine_files("meta.csv","result", metaResult)
    emit: 
        gwas
}
workflow polygenic_score_analyses{
    take: samples
    take: gwas
    main:
        // 1. First, remove all base samples as we won't be using them for PRSice
        pheno = samples \
            | filter{ a -> a[0].sample == "Target"}
        baseGWAS = gwas \
            | filter{ a -> a[0].sample == "Base"}
        base = combine_map(x: baseGWAS, y: pheno, by:0, column: ["perm", "herit", "numCausal", "sigmaS", "baseSize"])
        targetGWAS = gwas \
            | filter{ a -> a[0].sample != "Base"}
            
        combine_map(x: targetGWAS, y: pheno, by:0, column: ["perm", "herit", "numCausal", "overlap", "targetSize", "baseSize",  "sigmaS"]) \
            | mix(base) \
            | combine(genotype) \
            | combine(cov) \
            | run_prsice \
            | update_prsice \
            | groupTuple \
            | combine_tmp_files
        res = combine_tmp_files.out \
            | collect
        combine_prsice("ukb-fst-prs.csv", "result", res)\
            | combine(Channel.of("UKB.Fst")) \
            | combine(Channel.of("ukb-fst-prs.csv")) \
            | combine(Channel.of("result")) \
            | process_overlap_results
}

