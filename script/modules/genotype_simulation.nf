
process extract_haplotype{
    label 'haplotype'
    afterScript "ls * | grep -v legend | grep -v EUR | grep -v AFR | grep -v EAS | xargs rm "
    input:
        tuple   val(chr),
                path(hap),
                path(legend),
                path(sample),
                path(hapmap)
    output:
        tuple   val(chr),
                val("FIN"),
                path("${chr}.legend.gz"),
                path("${chr}-EUR-hap.gz"), emit: eur
        tuple   val(chr),
                val("YRI"),
                path("${chr}.legend.gz"),
                path("${chr}-AFR-hap.gz"), emit: afr
        tuple   val(chr),
                val("JPT"),
                path("${chr}.legend.gz"),
                path("${chr}-EAS-hap.gz"), emit: eas
    script:
    """
    split_haplotype ${legend} ${sample} ${hap} ${hapmap} ${chr}
    """
}


process run_hapgen{
    afterScript 'ls * | grep -v *.bed | grep -v *.bim | grep -v *.fam | xargs rm'
    label 'hapgen'
    input:
        tuple   val(chr),
                val(pop),
                path(legend),
                path(hap),
                path(recomb),
                val(seed)
    output:
        tuple   val(pop),
                path("${pop}-${chr}.bed"),
                path("${pop}-${chr}.bim"),
                path("${pop}-${chr}.fam")
    script:
    """
    zcat ${legend} > legend
    zcat ${hap} > hap
    zcat ${recomb} > recomb
    pos=`awk '\$9 > 0.1 && \$9 < 0.9 && NR != 1{print \$2}' legend | head -n 1`
    # Simulate 18k samples per population
    # Can't do it in batch or samples will be simulated as related samples
    sample=180000
    ne=11418
    if [[ "${pop}" == "YRI" ]]; then
        pos=`awk '\$6 > 0.1 && \$6 < 0.9 && NR != 1{print \$2}' legend | head -n 1`
        ne=17469;
    elif [[ "${pop}" == "JPT" ]]; then
        pos=`awk '\$8 > 0.1 && \$8 < 0.9 && NR != 1{print \$2}' legend | head -n 1`
        ne=14269;
    fi
    hapgen2 \
        -m recomb \
        -l legend \
        -h hap \
        -o ${pop}-${chr} \
        -n \${sample} 0 \
        -no_haps_output \
        -dl \${pos} 1 0 0 \
        -Ne \${ne}
    sed -i 's/id/${pop}/g' ${pop}-${chr}.controls.sample
    awk '{gsub(\$1,${chr},\$1); print}' ${pop}-${chr}.controls.gen > ${pop}-${chr}.gen
    # Immediately convert the gen file into bed files to reduce storage space requirement
    plink2 \
        --gen ${pop}-${chr}.gen ref-last \
        --sample ${pop}-${chr}.controls.sample \
        --make-bed \
        --out ${pop}-${chr} \
        --maf 0.01 \
        --threads ${task.cpus}
    """
}


process merge_genotype{
    label 'hapgen'
    input:
        path("*")
    output:
        tuple   path("genome.bed"),
                path("genome.bim"),
                path("genome.fam"), emit: geno
        path "genome.pop", emit: population
    script:
    """
    ls *bed | sed 's/.bed//g' > list
    plink \
        --merge-list list \
        --make-bed \
        --out genome \
        --allow-no-sex \
        --threads ${task.cpus} \
        --maf 0.01 \
        --hwe 1e-8 \
        --geno 0.02

    awk 'NR==1{print "FID","IID","Group"} /^FIN/ {print \$1,\$2,"EUR"} /^YRI/{print \$1, \$2,"AFR"}' genome.fam > genome.pop
    """
}


process calculate_pca{
    label 'long'
    afterScript "ls * | grep -v pcs.txt | xargs rm "
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
    output:
        path "pcs.txt"
    script:
    base = bed.baseName
    // Can do multi-threading
    """
    plink \
        --bfile ${base} \
        --indep-pairwise 200 50 0.2
    plink \
        --bfile ${base} \
        --extract plink.prune.in \
        --make-bed \
        --out temp
    flashpca \
        --bfile temp \
        -d 15
    """
}

process trim_geno_sample_for_ldsc{
    label 'normal'
    input:
        tuple   val(chr),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(chr),
                path("${chr}.bed"),
                path("${chr}.bim"),
                path("${chr}.fam")
    script:
    base = bed.baseName
    """
    # Use first 500 FIN and first 500 YRI sample for LDSC
    awk -v FIN=0 -v YRI=0 '{if(\$1 ~ /^FIN/ && FIN < 500){FIN++; print \$0} else if(\$1 ~ /^YRI/ && YRI < 500){YRI++; print \$0}}' ${fam} > keep
    plink \
        --bfile ${base} \
        --chr ${chr} \
        --keep keep \
        --out ${chr} \
        --make-bed
    """
}