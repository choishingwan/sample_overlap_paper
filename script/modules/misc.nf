
process combine_files{
    executor 'local'
    label 'normal'
    publishDir "${dir}", mode: 'copy'
    input:
        val(name)
        val(dir)
        path("*")
    output:
        path("${name}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i), fill=T)
    }
    fwrite(res, "${name}", sep = "\\t", na = "NA", quote = F)
    """
}
process combine_tmp_files{
    executor 'lsf'
    label 'short'
    input:
        tuple   val(name),
                path("*")
    output:
        path("${name}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i), fill=T)
    }
    fwrite(res, "${name}", sep = "\\t", na = "NA", quote = F)
    """
}

