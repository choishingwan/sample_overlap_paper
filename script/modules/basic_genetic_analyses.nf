include {   get_meta_value   }  from './handle_meta'
process genotype_filtering{
    label 'normal'
    input:
        tuple   val(chr),
                path(bed),
                path(bim),
                path(fam),
                path(pop),
                path(snp)
     output:
        tuple   val(chr),
                val("baseline"),
                path("chr${chr}-filtered.bed"),
                path("chr${chr}-filtered.bim"),
                path("chr${chr}-filtered.fam"), emit: baseline
        tuple   val(chr),
                val("weight"),
                path("chr${chr}-weight.bed"),
                path("chr${chr}-weight.bim"),
                path("chr${chr}-weight.fam"), emit: weight
    script:
    name=bed.baseName
    // Baseline should contain as much SNP as possible whereas weight should ideally only
    // include SNPs used for GWAS. 
    """
    awk '{print \$2}' ${bim} | sort| uniq -d > dup.snp
    plink \
        --bfile ${name} \
        --mac 5 \
        --geno 0.01 \
        --keep ${pop} \
        --make-bed \
        --out chr${chr}-filtered \
        --exclude dup.snp 
    plink \
        --bfile chr${chr}-filtered \
        --make-bed \
        --out chr${chr}-weight \
        --extract ${snp}
    """
}

process run_gwas{
    label 'threaded'
    afterScript 'ls * | grep -v glm | xargs rm'
    input: 
        tuple   val(meta),
                path(related),
                path(pheno),
                path(id),
                path(bed),
                path(bim),
                path(fam),
                path(snp),
                path(cov)
    output:
        tuple   val(meta),
                path("*glm*.gz")
    script:
    base=bed.baseName
    perm = get_meta_value(meta, "perm", 0)
    type = get_meta_value(meta, "type", "QT")
    related = get_meta_value(meta, "related", false)
    herit = get_meta_value(meta, "herit", 0)
    ascertain = get_meta_value(meta, "ascertain", "Random")
    sample = get_meta_value(meta, "sample", "Base")
    split = "-"+get_meta_value(meta, "split", "1")
    covar = get_meta_value(meta, "cov", "NA")
    extract = snp ? "--extract ${snp}" : ""
    """
    command="--glm"
    covar=""
    allow="allow-no-covars"
    if [[ "${covar}" != "NA" && "${type}" != "CC" ]];
    then
        covar="--covar ${cov} --covar-name ${covar}"
        allow="hide-covar"
    fi
    if [[ "${type}" == "CC" ]];
    then
        command=\$command" \${allow} cols=+ax,+a1freq,-ref,-alt,+orbeta \${covar} --1"
    else
        command=\$command" \${allow} cols=+ax,+a1freq,-ref,-alt,+orbeta \${covar} "
    fi
    plink2 \
        --bfile ${base} \
        --pheno ${pheno} \
        --pheno-name Pheno \
        --keep ${id} \
        --out ${perm}-${related}-${herit}-${type}-${ascertain}-${sample}${split} \
        --autosome \
        --threads ${task.cpus} \
        ${extract}\
        \${command}
    gzip *glm*
    """
}
