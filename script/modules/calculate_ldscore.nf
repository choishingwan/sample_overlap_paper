process calculate_ldscore{
    publishDir "data/reference/", mode: "${pub}"
    label 'normal'
    
    input:
        tuple   val(chr), 
                val(name),
                path(bed), 
                path(bim), 
                path(fam), 
                path(snp),
                val(pub)
    output:
        tuple   val("${name}"),
                path("${name}-${chr}.l2.ldscore.gz"), 
                path("${name}-${chr}.l2.M_5_50"), 
                path("${name}-${chr}.l2.M")
    script:
        base=bed.baseName
        printSnp = snp? "--print-snp ${snp}" : ""
        if(!name) name = "sim"
        """
        ldsc.py \
            --l2 \
            --bfile ${base} \
            --ld-wind-kb 1000 \
            --out ${name}-${chr} \
            ${printSnp}
        """
}
