include {   get_meta_value   }  from './handle_meta'
process modify_erasor_meta{
    label 'tiny'
    input:
        tuple   val(meta),
                path(inFile)
    output:
        tuple   val(meta),
                path ("${perm}-${type}-${related}-${herit}-${prevalence}-${ascertain}-${baseSize}-${targetSize}-${overlap}-${sigmaS}-${hasCov}-${shared}.res")
    script:
    perm = get_meta_value(meta, "perm", 0)
    type = get_meta_value(meta, "type", "QT")
    related = get_meta_value(meta, "related", false)
    herit = get_meta_value(meta, "herit", 0)
    ascertain = get_meta_value(meta, "ascertain", "Random")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    baseSize = get_meta_value(meta, "baseSize", 1000)
    targetSize = get_meta_value(meta, "targetSize", 1000)
    overlap = get_meta_value(meta, "overlap", 0)
    sigmaS = get_meta_value(meta, "sigmaS", 0)
    cov = get_meta_value(meta, "cov", "NA")
    shared = get_meta_value(meta, "shared", 0)
    hasCov = cov == "NA"?  false : true
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fread("${inFile}") %>%
        .[, Perm := ${perm}] %>%
        .[, Type := "${type}"] %>%
        .[, Related := "${related}"] %>%
        .[, Heritability := ${herit}] %>%
        .[, Ascertainment := "${ascertain}"] %>%
        .[, Prevalence := "${prevalence}"] %>%
        .[, BaseSize := ${baseSize}] %>%
        .[, TargetSize := ${targetSize}] %>%
        .[, Overlap := ${overlap}] %>%
        .[, SigmaS := ${sigmaS}] %>%
        .[, Covariate := "${cov}"] %>%
        .[, SharedEnvironment := "${shared}"] %>%
        fwrite(., "${perm}-${type}-${related}-${herit}-${prevalence}-${ascertain}-${baseSize}-${targetSize}-${overlap}-${sigmaS}-${hasCov}-${shared}.res")
    """
}

process combine_files{
    executor 'local'
    publishDir "${dir}", mode: 'copy', overwrite: true
    label 'normal'
    input:
        val(name)
        val(dir)
        path("*")
    output:
        path("${name}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i), fill=T)
    }
    fwrite(res, "${name}", sep = "\\t", na = "NA", quote = F)
    """
}

process combine_tmp_files{
    executor 'lsf'
    label 'normal'
    input:
        tuple   val(name),
                path("*")
    output:
        path("${name}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i), fill=T)
    }
    fwrite(res, "${name}", sep = "\\t", na = "NA", quote = F)
    """
}
process run_erasor{
    label 'normal'
    afterScript  "rm ${baseName}* ${weightName}*"
    container ""
    input:
        tuple   val(meta),
                path(targetGWAS),
                path(baseGWAS),
                val(baseName),
                path(baseline),
                val(weightName),
                path(weight),
                path(erasor)
    output:
        tuple   val(metaUpdate),
                path("${perm}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${targetSize}-${overlap}-${ascertain}-${shared}-adj.assoc.gz"), emit: gwas
        tuple   val(metaUpdate),
                path("${perm}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${targetSize}-${overlap}-${ascertain}-${shared}-adj.meta"), emit: info
        tuple   val(metaTruth),
                path("${perm}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${targetSize}-${overlap}-${ascertain}-${shared}-leblanc.assoc.gz") , emit: leblanc optional true
                
    script:
    metaUpdate = meta.clone()
    metaTruth = meta.clone()
    metaUpdate["sample"] = "Adjusted"
    metaTruth["sample"] = "LeBlanc"
    perm = get_meta_value(meta, "perm", 0)
    type = get_meta_value(meta, "type", "QT")
    related = get_meta_value(meta, "related", false)
    herit = get_meta_value(meta, "herit", 0)
    ascertain = get_meta_value(meta, "ascertain", "Random")
    sample = get_meta_value(meta, "sample", "Base")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    baseSize = get_meta_value(meta, "baseSize", 250000)
    targetSize = get_meta_value(meta, "targetSize", 1000)
    overlap = get_meta_value(meta, "overlap", 0.0)
    shared = get_meta_value(meta, "shared", 0)
    trueAdj = targetSize * overlap / Math.sqrt(baseSize * targetSize)
    test = baseSize * targetSize
    test2 = Math.sqrt(test)
    top = targetSize * overlap
    """
    stat="Z_STAT"
    ID="ID"
    if [[ "${type}" == "QT" ]];
    then
        stat="T_STAT"
    fi
    python ./EraSOR.py \
        --w-ld-chr ${weightName} \
        --ref-ld-chr ${baseName} \
        --base ${baseGWAS} \
        --target ${targetGWAS} \
        --out ${perm}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${targetSize}-${overlap}-${ascertain}-${shared}-adj \
        --base-N-col OBS_CT \
        --base-snp ID \
        --base-a2 AX \
        --base-p P \
        --base-signed-sumstats \${stat},0 \
        --same
    echo "${test} ${test2} ${top} ${trueAdj}"
    python ./EraSOR.py \
        --w-ld-chr ${weightName} \
        --ref-ld-chr ${baseName} \
        --base ${baseGWAS} \
        --target ${targetGWAS} \
        --out ${perm}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${targetSize}-${overlap}-${ascertain}-${shared}-leblanc \
        --base-N-col OBS_CT \
        --base-snp ID \
        --base-a2 AX \
        --base-p P \
        --base-signed-sumstats \${stat},0 \
        --leblanc ${trueAdj} \
        --same
    """
}

process run_prsice{
    label 'prsice'
    afterScript "rm *.best *.prsice"
    //maxForks '100'
    input:
        tuple   val(meta),
                path(sumstat),
                path(related),
                path(pheno),
                path(sample),
                path(bed),
                path(bim),
                path(fam),
                path(pcs)
    output:
        tuple   val(meta),
                path("${perm}.summary")

    script:
    base=bed.baseName
    perm = get_meta_value(meta, "perm", 0)
    type = get_meta_value(meta, "type", "QT")
    adjustment = get_meta_value(meta, "sample", "Base")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    cov = get_meta_value(meta, "cov", "NA")
    hasCov =  false
    if(cov != "NA"){
        hasCov = true
    }
    shared = get_meta_value(meta, "shared", 0)
    keep = sample? "--keep ${sample}": ""
    """
    binary=T
    stat="Z_STAT"
    ID="ID"
    prev="--prev ${prevalence}"
    cov=""
    if [[ "${type}" == "QT" ]];
    then
        binary=F
        prev=""
        stat="T_STAT"
    fi
    if [[ "${hasCov}" == "true" ]];
    then
        cov="--cov ${pcs} --cov-col ${cov}"
    fi
    if [[ "${adjustment}" == "Adjusted" || "${adjustment}" == "LeBlanc" ]];
    then
        stat="Z"
        ID="SNP"
    fi
    PRSice \
        --target ${base} \
        --pheno ${pheno} \
        --binary-target \${binary} \
        --base ${sumstat} \
        --thread ${task.cpus} \
        --snp \${ID} \
        --ultra \
        --out ${perm} \
        --stat \${stat} \
        --beta \
        ${keep} \
        \${prev} \
        \${cov}
    """
}


process update_lassosum{
    label 'tiny'
    input:
        tuple   val(meta),
                path(summary)
    output:
        tuple   val("${perm}"),
                path("lassosum-${perm}-${causal}-${stability}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${ascertain}-${overlap}-${targetSize}-${sample}-${shared}-${sigmaS}-${cov}")
    script:
    perm = get_meta_value(meta, "perm", 0)
    type = get_meta_value(meta, "type", "QT")
    related = get_meta_value(meta, "related", false)
    herit = get_meta_value(meta, "herit", 0)
    ascertain = get_meta_value(meta, "ascertain", "Random")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    baseSize = get_meta_value(meta, "baseSize", 1000)
    targetSize = get_meta_value(meta, "targetSize", 1000)
    overlap = get_meta_value(meta, "overlap", 0)
    sample = get_meta_value(meta, "sample", "Target")
    sigmaS = get_meta_value(meta, "sigmaS", 0)
    shared = get_meta_value(meta, "shared", 0)
    cov = get_meta_value(meta, "cov", "NA")
    stability = get_meta_value(meta, "stability", 1)
    causal = get_meta_value(meta, "numCausal", 10000)
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    select <- c("Software","PRS.R2", "P")
    sample <- "${sample}"
    if(sample == "Base") sample <- "Unadjusted"
    fread("${summary}") %>%
        .[is.na(PRS.R2), PRS.R2 := PRS.R2.value] %>%
        .[,select, with = F] %>%
        .[, Perm := ${perm}] %>%
        .[, NumCausal := ${causal}] %>%
        .[, Related := "${related}"] %>%
        .[, Trait := "${type}"] %>%
        .[, Heritability := ${herit}] %>%
        .[, Prevalence := ${prevalence}] %>%
        .[, BaseSize := ${baseSize}] %>%
        .[, TargetSize := ${targetSize}] %>%
        .[, Overlap := ${overlap}] %>%
        .[, Ascertainment := "${ascertain}"] %>%
        .[, SampleType := sample] %>%
        .[, SigmaS := "${sigmaS}"] %>%
        .[, SharedEnv := "${shared}"] %>%
        .[, Cov := "${cov}"] %>%
        .[, ErrorCor := ${stability}] %>%
        fwrite(., "lassosum-${perm}-${causal}-${stability}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${ascertain}-${overlap}-${targetSize}-${sample}-${shared}-${sigmaS}-${cov}")
    """
}
process update_prsice{
    label 'tiny'
    input:
        tuple   val(meta),
                path(summary)
    output:
        tuple   val("${perm}"),
                path("PRSice-${perm}-${causal}-${stability}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${ascertain}-${overlap}-${targetSize}-${sample}-${shared}-${sigmaS}-${cov}")
    script:
    perm = get_meta_value(meta, "perm", 0)
    type = get_meta_value(meta, "type", "QT")
    related = get_meta_value(meta, "related", false)
    herit = get_meta_value(meta, "herit", 0)
    ascertain = get_meta_value(meta, "ascertain", "Random")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    baseSize = get_meta_value(meta, "baseSize", 1000)
    targetSize = get_meta_value(meta, "targetSize", 1000)
    overlap = get_meta_value(meta, "overlap", 0)
    sample = get_meta_value(meta, "sample", "Target")
    sigmaS = get_meta_value(meta, "sigmaS", 0)
    shared = get_meta_value(meta, "shared", 0)
    cov = get_meta_value(meta, "cov", "NA")
    stability = get_meta_value(meta, "stability", 1)
    causal = get_meta_value(meta, "numCausal", 10000)
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    select <- c("PRS.R2", "Coefficient", "Standard.Error", "P")
    if("${type}" == "CC"){
        select <- c(select, "PRS.R2.adj")
    }
    sample <- "${sample}"
    if(sample == "Base") sample <- "Unadjusted"
    fread("${summary}") %>%
        .[,select, with = F] %>%
        .[, Perm := ${perm}] %>%
        .[, Software := "PRSice"] %>%
        .[, NumCausal := ${causal}] %>%
        .[, Related := "${related}"] %>%
        .[, Trait := "${type}"] %>%
        .[, Heritability := ${herit}] %>%
        .[, Prevalence := ${prevalence}] %>%
        .[, BaseSize := ${baseSize}] %>%
        .[, TargetSize := ${targetSize}] %>%
        .[, Overlap := ${overlap}] %>%
        .[, Ascertainment := "${ascertain}"] %>%
        .[, SampleType := sample] %>%
        .[, SigmaS := "${sigmaS}"] %>%
        .[, SharedEnv := "${shared}"] %>%
        .[, Cov := "${cov}"] %>%
        .[, ErrorCor := ${stability}] %>%
        fwrite(., "PRSice-${perm}-${causal}-${stability}-${related}-${type}-${herit}-${prevalence}-${baseSize}-${ascertain}-${overlap}-${targetSize}-${sample}-${shared}-${sigmaS}-${cov}")
    """
}

process process_overlap_results{
    label 'short'
    publishDir "${dir}", mode: 'copy', overwrite: true
    input:
        tuple   path(res),
                val(analysis),
                val(output),
                val(dir)
    output:
        path("${output}")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    dat <- fread("${res}")[, Analysis := "${analysis}"]
    if(any(colnames(dat) %in% c("PRS.R2.adj"))){
        dat <- dat[,-c("PRS.R2.adj")]
    }
    if(any(colnames(dat) %in% c("ErrorCor"))){
        dat <- dat[,-c("ErrorCor")]
    }
    # To save time, we have skipped some analysis that'd generate identical results
    # which we will need to account for when we generate the plots
    # Extract the expected data, which is when we don't have sample overlap
    unadjusted <- dat[SampleType == "Unadjusted" & Overlap == 0]
    # Now we need to account for situations where we've skipped
    # First, we didn't recalculate the expected results when overlap = 0,
    # as the sample set should  be identical to the base
    expected <- copy(unadjusted)[, SampleType := "Expected"]
    
    # Now add back the missing data
    dat <- rbind(dat, expected)
    # For case control analyses, we also ignore case overlap and control overlap
    # when overlap == 0, as they'd be identical
    if("CC" %in% dat[,Trait]){
        random.null <-
            copy(unadjusted[Ascertainment == "Random" & Trait == "CC"])
        # We need to regenerate the required samples
        nonrandom.expected <-
            rbind(  copy(random.null)[, Ascertainment := "Case"],
                    copy(random.null)[, Ascertainment := "Control"]) %>%
            .[, SampleType := "Expected"]
        # For adjusted results with overlap == 0, case overlap and control overlap
        # should be the same as the random overlap
        random.adjusted <-
            copy(dat[   Ascertainment == "Random" &
                        Overlap == 0 & 
                        Trait == "CC"])
        random.adjusted.all <-
            rbind(  copy(random.adjusted)[, Ascertainment := "Case"],
                    copy(random.adjusted)[, Ascertainment := "Control"])
        # Now add back the missing data
        dat <- rbind(dat, nonrandom.expected) %>%
            rbind(., random.adjusted.all)
    }
    dat <- unique(dat)
    # Remove columns and rename
    expected.ref <- dat[SampleType == "Expected"] %>%
        .[, Ref.Z := Coefficient / Standard.Error] %>%
        .[, -c("Coefficient",
                "Standard.Error",
                "SampleType")] %>%
        setnames(., c("PRS.R2", "P"), c("Ref.R2", "Ref.P"))

    unadjusted.ref <- dat[SampleType == "Unadjusted" & Overlap == 0] %>%
        .[, Ref.Z := Coefficient / Standard.Error] %>%
        .[, -c("Coefficient",
                "Standard.Error",
                "Overlap")] %>%
        setnames(., c("PRS.R2", "P"), c("Ref.R2", "Ref.P"))
    # Now get the results by merging the data
    dat[, Z := Coefficient / Standard.Error]
    adjusted <- merge(  
        dat[SampleType != "Unadjusted"],
        expected.ref,
        by = c(
            "Software",
            "NumCausal",
            "Perm",
            "Related",
            "Trait",
            "Heritability",
            "Prevalence",
            "BaseSize",
            "TargetSize",
            "Ascertainment",
            "Overlap",
            "SigmaS",
            "SharedEnv",
            "Cov",
            "Analysis"
            )
    )
    unadjusted.results <- merge(
        unadjusted.ref,
        dat,
        by = c(
            "Software",
            "NumCausal",
            "Perm",
            "Related",
            "Trait",
            "Heritability",
            "Prevalence",
            "BaseSize",
            "TargetSize",
            "Ascertainment",
            "SigmaS",
            "SharedEnv",
            "SampleType",
            "Cov",
            "Analysis"
        )
    )
    overall.results <-
        rbind(  unadjusted.results,
                adjusted) %>%
        .[, PRS.Ratio := (PRS.R2 - Ref.R2) / Ref.R2] %>%
        .[, PRS.Diff := (PRS.R2 - Ref.R2)] %>%
        .[, P.Ratio := (-log10(P)--log10(Ref.P)) / -log10(Ref.P)] %>%
        .[, P.Diff := (-log10(P)--log10(Ref.P))] %>%
        .[, BaseBinarySize := (BaseSize / (Prevalence * (1 - Prevalence))) / 4] %>%
        .[, TargetBinarySize := (TargetSize / (Prevalence * (1 - Prevalence))) / 4] %>%
        .[ Trait == "CC", BaseSize := BaseBinarySize] %>%
        .[ Trait == "CC", TargetSize := TargetBinarySize] %>%
        .[, -c("BaseBinarySize", "TargetBinarySize")] %>%
        .[, Cov := ifelse(is.na(Cov), "NoPC", "PC")] %>%
        .[, Severity := (Overlap * TargetSize * ifelse(
            Ascertainment == "Random",
            1,
            ifelse(Ascertainment == "Case", Prevalence, 1 - Prevalence)
        ) / (sqrt(BaseSize) * sqrt(TargetSize)))] %>%
        .[ SampleType != "Expected"]
    fwrite(overall.results, "${output}")
    """
}


process process_real_overlap_results{
    label 'short'
    publishDir "${dir}", mode: 'copy', overwrite: true
    input:
        tuple   path(res),
                val(analysis),
                val(output),
                val(dir)
    output:
        path("${output}")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    # We have used Ascertainment to store our trait ID
    dat <-
        fread("${res}")[, Analysis := "${analysis}"] %>%
        .[, Trait := Ascertainment]
    if(any(colnames(dat) %in% c("PRS.R2.adj"))){
        dat <- dat[,-c("PRS.R2.adj")]
    }
    if(any(colnames(dat) %in% c("ErrorCor"))){
        dat <- dat[,-c("ErrorCor")]
    }
    dat %<>%
    # Remove all place holder columns to make the result cleaner
        .[, -c("Ascertainment",
            "Related",
            "Heritability",
            "Prevalence",
            "SigmaS",
            "SharedEnv",
            "Cov")]
    unadjusted <- dat[SampleType == "Unadjusted" & Overlap == 0]
    expected <- copy(unadjusted)[, SampleType := "Expected"]
    dat <- rbind(dat, expected) %>%
        unique
    # Remove columns and rename
    expected.ref <- dat[SampleType == "Expected"] %>%
        .[, Ref.Z := Coefficient / Standard.Error] %>%
        .[, -c("Coefficient",
                "Standard.Error",
                "SampleType")] %>%
        setnames(., c("PRS.R2", "P"), c("Ref.R2", "Ref.P"))
    unadjusted.ref <- dat[SampleType == "Unadjusted" & Overlap == 0] %>%
        .[, Ref.Z := Coefficient / Standard.Error] %>%
        .[, -c("Coefficient",
                "Standard.Error",
                "Overlap")] %>%
        setnames(., c("PRS.R2", "P"), c("Ref.R2", "Ref.P"))
    # Now get the results by merging the data
    dat[, Z := Coefficient / Standard.Error]
    adjusted <- merge(  
        dat[SampleType != "Unadjusted"],
        expected.ref,
        by = c(
            "Software",
            "Perm",
            "Trait",
            "BaseSize",
            "TargetSize",
            "Overlap",
            "Analysis"
            )
    )
    unadjusted.results <- merge(
        unadjusted.ref,
        dat,
        by = c(
            "Software",
            "Perm",
            "Trait",
            "BaseSize",
            "TargetSize",
            "SampleType",
            "Analysis"
        )
    )
    overall.results <-
        rbind(  unadjusted.results,
                adjusted) %>%
        .[, PRS.Ratio := (PRS.R2 - Ref.R2) / Ref.R2] %>%
        .[, PRS.Diff := (PRS.R2 - Ref.R2)] %>%
        .[, P.Ratio := (-log10(P)--log10(Ref.P)) / -log10(Ref.P)] %>%
        .[, P.Diff := (-log10(P)--log10(Ref.P))] %>%
        .[, Severity := (Overlap * TargetSize) / (sqrt(BaseSize) * sqrt(TargetSize))] %>%
        .[ SampleType != "Expected"]
    fwrite(overall.results, "${output}")
    """
}
process run_lassosum{
    label 'lassosum'
    module 'plink'
    module 'R'
    //maxForks '100'
    afterScript  "rm ${perm}.bed ${perm}.bim ${perm}.fam"
    input:
        tuple   val(meta),
                path(sumstat),
                path(related),
                path(pheno),
                path(sample),
                path(bed),
                path(bim),
                path(fam),
                path(pcs),
                path(lassosum)
    output:
        tuple   val(meta),
                path("${perm}.result")

    script:
    base=bed.baseName
    perm = get_meta_value(meta, "perm", 0)
    type = get_meta_value(meta, "type", "QT")
    adjustment = get_meta_value(meta, "sample", "Base")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    cov = get_meta_value(meta, "cov", "NA")
    baseSize = get_meta_value(meta, "baseSize", 1000)
    hasCov =  false
    if(cov != "NA"){
        hasCov = true
    }
    shared = get_meta_value(meta, "shared", 0)
   
    """
    binary=T
    stat="Z_STAT"
    ID="ID"
    prev="--prev ${prevalence}"
    cov=""
    AX="AX"
    N="--Ncol OBS_CT"
    if [[ "${type}" == "QT" ]];
    then
        binary=F
        prev=""
        stat="T_STAT"
    fi
    if [[ "${hasCov}" == "true" ]];
    then
        cov="--covFile ${pcs} --covar ${cov}"
    fi
    if [[ "${adjustment}" == "Adjusted" || "${adjustment}" == "LeBlanc" ]];
    then
        stat="Z"
        ID="SNP"
        AX="A2"
        N="--N ${baseSize}"
    fi
    plink \
        --bfile ${base} \
        --keep ${sample} \
        --make-bed \
        --out ${perm}

    Rscript ${lassosum} \
        --target ${perm} \
        --base ${sumstat} \
        --pheno ${pheno} \
        --thread ${task.cpus} \
        --name Pheno \
        --snp \${ID} \
        --out ${perm} \
        --stat \${stat} \
        --reference \${AX} \
        \${cov} \
        \${N}
    """
}