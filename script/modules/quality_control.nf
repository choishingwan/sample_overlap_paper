
process extract_biological_sex{
    // We don't extract centre as the centre can change depending on the instance
    label 'short'
    input:
        path(sql)
    output:
        path("bioSex")
    script:
    """
    echo "
    .mode csv
    .header on
    .output bioSex
    
    SELECT      s.sample_id AS FID,
                s.sample_id AS IID,
                sex.pheno AS Sex
    FROM        Participant s
    JOIN        f31 sex ON
                sex.instance = 0 AND
                sex.sample_id = s.sample_ID
    WHERE       s.withdrawn = 0;
    .quit
        " > sql;
    sqlite3 ${sql} < sql
    """
}

process outliers_aneuploidy_related{
    label 'short'
    input:
        path(sql)
    output:
        path "outliers", emit: outliers
    script:
    """
    echo "
    .mode csv
    .header on
    .output outliers
    CREATE TEMP TABLE problematic
    AS
    SELECT DISTINCT sample_id 
    FROM(
        SELECT  sample_id
        FROM    f22019 aneuploidy 
        WHERE   aneuploidy.pheno = 1 AND
                aneuploidy.instance = 0
        UNION 
        SELECT  sample_id
        FROM    f22027 outlier 
        WHERE   outlier.pheno = 1 AND
                outlier.instance = 0
        UNION 
        SELECT sample_id
        FROM    f22021 related
        WHERE   related.pheno = 10 AND
                related.instance = 0
    )as subquery;

    SELECT  s.sample_id AS FID,
            s.sample_id AS IID
    FROM    Participant s
    JOIN    problematic ON
            s.sample_id = problematic.sample_id AND
            s.withdrawn = 0;
    .quit
        " > sql;
    sqlite3 ${sql} < sql
    """
}
process remove_dropout_and_invalid{
    label 'short'
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
        path(invalid)
        val(dropout)
    output:
        path "remove.samples", emit: removed
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fam <- fread("${fam}") %>%
        setnames(., c("V1", "V2"),c("FID", "IID")) %>%
        .[,c("FID", "IID")]
    invalid <- fread("${invalid}")   
    num.drop <- 0
    
    if("${dropout}" != "null"){     
        dropout <- fread("${dropout}", header=F)
        fam <- fam[IID%in% dropout[,V1]]
        num.drop <- nrow(dropout)
        invalid <- rbind(invalid, fam)
    }
    fwrite(invalid, "remove.samples", sep="\\t")
    """
}

process basic_qc{
    label 'normal'
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
        path(pop)
        path(remove)
        val(hwe)
        val(geno)
        val(maf)
    output:
        tuple   path("basic-qc.fam"), 
                path("basic-qc.snplist"), emit: qc
    script:
    base=bed.baseName
    """
    plink   --keep ${pop} \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --geno ${geno} \
            --maf ${maf} \
            --hwe ${hwe} \
            --write-snplist \
            --make-just-fam \
            --out basic-qc \
            --remove ${remove} \
            --threads ${task.cpus}
    """
}

process prunning{
    label 'normal'
    input: 
        tuple   path(bed),
                path(bim),
                path(fam)
        tuple   path(qc_fam),
                path(qc_snp)
        path(high_ld)
        val(wind_size)
        val(wind_step)
        val(wind_r2)
        val(max_size)
        val(seed)
    output:
        path "qc.prune.in"
    script:
    base=bed.baseName
    """
    if [[ \$(wc -l < ${qc_fam}) -ge ${max_size} ]];
    then
        plink \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --extract ${qc_snp} \
            --keep ${qc_fam} \
            --indep-pairwise ${wind_size} ${wind_step} ${wind_r2} \
            --out qc \
            --thin-indiv-count ${max_size} \
            --seed ${seed} \
            --exclude ${high_ld}
    else
        plink \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --extract ${qc_snp} \
            --keep ${qc_fam} \
            --indep-pairwise ${wind_size} ${wind_step} ${wind_r2} \
            --out qc \
            --exclude ${high_ld}
    fi 
    """
}  

process generate_high_ld_region{
    label 'normal'
    input:
        tuple   path(qc_fam), 
                path(qc_snp)
        tuple   path(bed),
                path(bim),
                path(fam)
        val(build)
    output:
        path "high_ld.set"
    script:
    base=bed.baseName
    """
    echo "1     48000000     52000000   High_LD
2     86000000     100500000    High_LD
2     134500000     138000000   High_LD
2     183000000     190000000   High_LD
3     47500000     50000000 High_LD
3     83500000     87000000 High_LD
3     89000000     97500000 High_LD
5     44500000     50500000 High_LD
5     98000000     100500000    High_LD
5     129000000     132000000   High_LD
5     135500000     138500000   High_LD
6     25000000     35000000 High_LD
6     57000000     64000000 High_LD
6     140000000     142500000   High_LD
7     55000000     66000000 High_LD
8     7000000     13000000  High_LD
8     43000000     50000000 High_LD
8     112000000     115000000   High_LD
10     37000000     43000000    High_LD
11     46000000     57000000    High_LD
11     87500000     90500000    High_LD
12     33000000     40000000    High_LD
12     109500000     112000000  High_LD
20     32000000     34500000 High_LD" > high_ld_37
    echo "1     48060567     52060567     hild
2     85941853     100407914     hild
2     134382738     137882738     hild
2     182882739     189882739     hild
3     47500000     50000000     hild
3     83500000     87000000     hild
3     89000000     97500000     hild
5     44500000     50500000     hild
5     98000000     100500000     hild
5     129000000     132000000     hild
5     135500000     138500000     hild
6     25500000     33500000     hild
6     57000000     64000000     hild
6     140000000     142500000     hild
7     55193285     66193285     hild
8     8000000     12000000     hild
8     43000000     50000000     hild
8     112000000     115000000     hild
10     37000000     43000000     hild
11     46000000     57000000     hild
11     87500000     90500000     hild
12     33000000     40000000     hild
12     109521663     112021663     hild
20     32000000     34500000     hild
X     14150264     16650264     hild
X     25650264     28650264     hild
X     33150264     35650264     hild
X     55133704     60500000     hild
X     65133704     67633704     hild
X     71633704     77580511     hild
X     80080511     86080511     hild
X     100580511     103080511     hild
X     125602146     128102146     hild
X     129102146     131602146     hild" > high_ld_38
    ldFile=high_ld_37
    if [[ "${build}" != "grch37" ]];
    then
        ldFile=high_ld_38
    fi
    echo \${ldFile}
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${qc_snp} \
        --keep ${qc_fam} \
        --make-set \${ldFile} \
        --write-set \
        --out high_ld
    """
}

process calculate_stat_for_sex{
    label 'normal'
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
        tuple   path(qc_fam),
                path(qc_snp)
        path(prune)
    output:
        path "plink.sexcheck"
    script:
    base=bed.baseName
    """
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${prune} \
        --keep ${qc_fam} \
        --check-sex 
    """
}


process filter_sex_mismatch{
    label 'short'
    input:
        tuple   path(qc_fam), 
                path(qc_snp)
        path (fstat)
        path (biosex)
    output:
        path "sex-mismatch", emit: mismatch
        path "sex-valid", emit: valid
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    # Original script allow for different way of filtering, but for our simulation we will 
    # just use 3 SD away from the mean
    fam <- fread("${qc_fam}")
    # Read in sex information and remove samples that doesn't pass QC
    sex <- fread("${biosex}") %>%
        merge(., fread("${fstat}")) %>%
        .[FID %in% fam[,V1] & FID > 0]
        sex.bound <- sex[,.(m=mean(F), s=sd(F)), by="Sex"]
        sex[,invalid := FALSE]
        bound <- sex.bound[Sex=="M"]
        sex[   Sex=="M" &
            ( F < bound[,m] -bound[,s]*3 ), invalid:=TRUE]
        bound <- sex.bound[Sex=="F"]
        sex[   Sex=="F" &
            (F > bound[,m] + bound[,s]*3), invalid:=TRUE]
    
    invalid <- sex[invalid==TRUE]
    fwrite(invalid, "sex-mismatch", sep="\\t")
    fwrite(sex[invalid==FALSE], "sex-valid", sep="\\t")
    """
}


process relatedness_filtering{
    label 'normal'
    input: 
        path(greedy)
        path(related)
        path(samples) 
        val(thres)
        val(seed)
    output:
        path "invalid.samples", emit: removed

    script:
    """
    ./${greedy} \
        -r ${related} \
        -i ID1 \
        -I ID2 \
        -f Kinship \
        -k ${samples} \
        -o invalid.samples \
        -t ${thres} \
        -s ${seed}
    """
}

process finalize_data{
    publishDir "genotyped", mode: 'copy', overwrite: true, pattern: "*snplist"
    publishDir "genotyped", mode: 'copy', overwrite: true, pattern: "*qc.fam"
    label 'normal'
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
        tuple   path(qc_fam),
                path(qc_snp)
        path(sex)
        path(rel)
    output:
        tuple   path("admix-qc.snplist"),
                path("admix-qc.fam"), emit: qced
    script:
    base=bed.baseName
    """
    cat ${rel} ${sex} > admix.removed
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${qc_snp} \
        --keep ${qc_fam} \
        --remove admix.removed \
        --make-just-fam \
        --write-snplist \
        --out admix-qc
    rm ${qc_fam}
    """
}