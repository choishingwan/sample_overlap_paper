include {   get_meta_value   }  from './handle_meta'
process extract_valid_samples{
    label 'tiny'
    input:
        tuple   path(fam),
                path(withdrawn),
                path(parents),
                path(siblings)
    output:
        tuple   val(unrelatedMeta),
                path("valid.fam"), 
                path("stub"), emit: unrelated
        tuple   val(relatedMeta),
                path("related.fam"),
                path("related.info"), emit: related

    script:
    relatedMeta = [related: true]
    unrelatedMeta = [related: false]
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    fam <- fread("${fam}")
    withdrawn <- fread("${withdrawn}", header=F)
    fam[!V2 %in% withdrawn[,V1]] %>%
        fwrite(., "valid.fam", sep="\\t")
    # This is for place holder which helps to make downstream
    # scripts slightly more reusable
    fwrite(withdrawn, "stub")
    # Now process the related samples
    # We need to include siblings as we don't have enough parents
    # ID1 is the index and are usually the samples included in the
    # Post QC file
    # On the other hand, ID2 is usually removed, so we only check
    # if they have withdrawn their consent
    related <- fread("${parents}") %>%
        rbind(., fread("${siblings}")) %>%
        .[ID1 %in% fam[,V2]] %>%
        .[!ID1 %in% withdrawn[,V1]] %>%
        .[!ID2 %in% withdrawn[,V1]]  
    # Ensure each index sample only have one sibling or parent
    problem.id2 <- related[duplicated(ID2), c("ID2")]
    # Preferrentially keep parents (min(IBS0)) over siblings
    related %<>%
        .[!ID2 %in% problem.id2[,ID2]] %>%
        .[, .SD[IBS0==min(IBS0, na.rm=T)][sample(.N, 1)], by="ID1"]
    related %>%
        fwrite(., "related.info", sep="\\t")
    # now generate the mock fam file
    data.table(V1=related[,ID2], V2=related[,ID2]) %>%
        rbind(., fam[,c("V1", "V2")]) %>%
        unique %>%
        fwrite(., "related.fam", sep="\\t")
    """
} 


process calculate_xbeta{
    label 'normal'
    input:
        tuple   val(meta), 
                path(samples),
                path(relatedInfo),
                path(bed),
                path(bim),
                path(fam),
                path(snp)
    output:
        tuple   val(meta),
                path(relatedInfo),
                path("${perm}-${related}.xbeta")
    script:
    base = bed.baseName
    related = get_meta_value(meta, "related", "Unrelated")
    perm = get_meta_value(meta, "perm", 0)
    seed = get_meta_value(meta, "seed", 1234)
    causal = get_meta_value(meta, "numCausal", 10000)
    keep = samples ? "--keep ${samples}" : ""
    """
    
    BBS \
        --input ${base} \
        --effect 2 \
        --extract ${snp} \
        --out ${perm}-${related} \
        --seed ${seed} \
        --herit 0.8 \
        --nsnp ${causal} \
        --std \
        ${keep}
    """
}

process generate_phenotype{
    label 'short'
    input:
        tuple   val(meta),
                path(relatedInfo),
                path(xbeta),
                path(population)
    output:
        tuple   val(metaUpdate),
                path(relatedInfo),
                path("${perm}-${herit}.pheno")
    script:
    metaUpdate =  meta.clone()
    metaUpdate["phenoName"] = "Pheno"
    perm = get_meta_value(meta, "perm", 0)
    seed = get_meta_value(meta, "seed", 1234)
    herit = get_meta_value(meta, "herit", 0)
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    type = get_meta_value(meta, "type", "QT")
    related = get_meta_value(meta, "related", false)
    shared = get_meta_value(meta, "shared", 0)
    sigmaS = get_meta_value(meta, "sigmaS", 0)
    has_pop = "F"
    if(population){
        has_pop = "T"
    }else{
        has_pop = "F"
        sigmaS = 0
    }
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    set.seed(${seed})
    xbeta <- fread("${xbeta}", header = T)
    colnames(xbeta)[3] <- "XB"
    xbeta <- xbeta %>%
        na.omit %>%
        .[, XB := (XB - mean(XB)) / sd(XB) * sqrt(${herit})] %>%
        .[, Env := 0]
    if(${has_pop}){
         xbeta %<>% 
            merge(., fread("${population}")) %>%
            .[, Env := -sqrt(${sigmaS})/2] %>%
            .[!Group %like% "^EUR", Env := -Env]
    }
    xbeta[, Shared := rnorm(.N, sd = sqrt(${shared}))]
    if("${related}" == "true"){
        related <- fread("${relatedInfo}") %>%
            merge(., xbeta, by.x="ID1", by.y="IID") %>%
            .[, c("ID2", "Shared")] %>%
            setnames(., c("ID2", "Shared"), c("IID", "Relative")) %>%
            merge(xbeta, ., all.x = T, by="IID") %>%
            .[!is.na(Relative), Shared := Relative]
        xbeta <- related
    }
    if(${herit} + ${sigmaS} + ${shared}  > 0){
        xbeta[, Noise := rnorm(.N, sd = sqrt((var(XB + Env - 2* cov(XB, Env) + ${shared})) * (1-${herit}-${sigmaS}-${shared})/(${herit} + ${sigmaS} + ${shared})))]
    }else{
        xbeta[, Noise := rnorm(.N)]
    }
    xbeta <- xbeta %>%
        .[, Pheno := Noise + XB + Shared + Env] %>%
        .[, Pheno := (Pheno - mean(Pheno)) / sd(Pheno)]
    if ("${type}" == "CC") {
        xbeta[, Pheno := as.numeric(Pheno > qnorm(${prevalence}, lower.tail = F))]
    }else{
        xbeta[, Pheno := (Pheno + rnorm(1, sd = 100))*sqrt(runif(1, min=1,max=100))]
    }
    fwrite(xbeta[, c("FID", "IID", "Pheno")], "${perm}-${herit}.pheno",
        sep = "\\t")
    """
}

process select_base_samples{
    label 'tiny'
    input:
        tuple   val(meta),
                path(relatedInfo),
                path(pheno),
                path(population)
    output:
        tuple   val(updatedMeta),
                path(relatedInfo),
                path(pheno),
                path("${perm}-${herit}.id")
    script:
    updatedMeta = meta.clone()
    updatedMeta["sample"] = "Base"
    seed = get_meta_value(meta, "seed", 1234)
    type = get_meta_value(meta, "type", "QT")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    baseSize = get_meta_value(meta, "baseSize", 250000)
    perm = get_meta_value(meta, "perm", 0)
    related = get_meta_value(meta, "related", false)
    herit = get_meta_value(meta, "herit", 0)
    has_pop = population ? "T" : "F"
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    set.seed(${seed})
    # Read in all phenotype
    pheno <- fread("${pheno}")
    effective.sample.size <- function(prevalence, sample.size) {
        return(floor(sample.size / (4 * prevalence * (1 - prevalence))))
    }
    if ("${type}" == "CC") {
        # Need to select samples base on the effective sample size
        size <- effective.sample.size(${prevalence}, ${baseSize})
        ncases <- ceiling(size * ${prevalence})
        ncontrols <- ceiling(size - ncases)
        pheno <-
            pheno[, .SD[sample(.N, ifelse(Pheno == 1, ncases, ncontrols))] ,
                  by = Pheno]
    } else if ("${related}" == "true") {
        # For related samples, always use maximum amount of related samples
        related <- fread("${relatedInfo}")
        index.samples <- pheno[IID %in% related[, ID1] & !IID %in% related[,ID2]]
        unrelated.samples <-
            pheno[!(IID %in% related[, ID1] |
                        IID %in% related[, ID2])]
        size <- nrow(index.samples)
        pheno <- unrelated.samples[sample(.N, ${baseSize} - size)] %>%
            rbind(index.samples)
    } else{
        # We only allow population simulation with non-related QT samples
        # Just plain QT selection
        if(${has_pop}){
            # We want the sample split represent the proportion of the population groups
            pheno <- merge(pheno, fread("${population}"))
            # Do two stage, just in case we drop some sample when we add the population information

            pheno[,selected := sample(c(    rep(1, floor(${baseSize} * .N/nrow(pheno))),
                                            rep(0, .N - floor(${baseSize} * .N/nrow(pheno))))), by = Group]
            pheno <- pheno[selected==1]
        }else{
            pheno <- pheno[sample(.N, ${baseSize})]
        }
    }
    fwrite(pheno[, c("FID", "IID")],
           "${perm}-${herit}.id",
           sep = "\\t")
    """
}

process select_target_samples{
    label 'tiny'
    input:
        tuple   val(meta),
                path(relatedInfo),
                path(pheno),
                path(baseSample),
                path(population)
    output:
        tuple   val(metaTarget),
                path(relatedInfo),
                path(pheno),
                path("${perm}-${herit}-${related}-${ascertain}.target"), emit: target
        tuple   val(metaExpected),
                path(relatedInfo),
                path(pheno),
                path("${perm}-${herit}-${related}-${ascertain}.expected"), emit: expected
    script:
    metaTarget = meta.clone()
    metaTarget["sample"] = "Target"
    metaExpected = meta.clone()
    metaExpected["sample"] = "Expected"
    perm = get_meta_value(meta, "perm", 0)
    herit = get_meta_value(meta, "herit", 0)
    related = get_meta_value(meta, "related", false)
    ascertain = get_meta_value(meta, "ascertain", "Random")
    overlap = get_meta_value(meta, "overlap", 0)
    targetSize = get_meta_value(meta, "targetSize", 1000)
    type = get_meta_value(meta, "type", "QT")
    prevalence = get_meta_value(meta, "prevalence", 0.1)
    has_pop = population ? "T" : "F"
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    # Read in all phenotype
    pheno <- fread("${pheno}")
    base <- fread("${baseSample}")
    effective.sample.size <- function(prevalence, sample.size) {
        return(floor(sample.size / (4 * prevalence * (1 - prevalence))))
    }
    unique <- NULL
    target <- NULL
    expected <- NULL
    if ("${type}" == "CC") {
        target.size <- effective.sample.size(${prevalence}, ${targetSize})
        num.cases <- ceiling(target.size *${prevalence})
        num.controls <- target.size - num.cases
        num.overlap.cases <- 0
        num.overlap.controls <- 0
        num.unique.cases <- 0
        num.unique.controls <- 0
        if ("${ascertain}" == "Random") {
            num.overlap.cases <- ceiling(num.cases * ${overlap})
            num.overlap.controls <- ceiling(num.controls * ${overlap})
        } else{
            case.overlap.ratio <-
                ifelse("${ascertain}" == "Case", ${overlap}, 0)
            num.overlap.cases <- ceiling(num.cases * case.overlap.ratio)
            num.overlap.controls <-
                ceiling(num.controls * (${overlap} - case.overlap.ratio))
        }
        num.unique.cases <- num.cases - num.overlap.cases
        num.unique.controls <- num.controls - num.overlap.controls
        overlapped <-
            pheno[IID %in% base[, IID]] %>%
            .[, .SD[sample(.N,
                    ifelse(Pheno == 1, num.overlap.cases, num.overlap.controls))], 
                    by = Pheno]
        unique <-
            pheno[!IID %in% base[, IID]] %>%
            .[, .SD[sample(.N, ifelse(Pheno == 1, num.unique.cases, num.unique.controls))],
                by = Pheno]
        target <- rbind(unique, overlapped)
        expected <- base[!IID %in% target[,IID]]
    } else if ("${related}" == "true") {
        related <- fread("${relatedInfo}")
        num.related <- ceiling(${targetSize} * ${overlap})
        num.unique <-${targetSize} - num.related
        related.target <-
            pheno[IID %in% related[, ID2] &
                      !IID %in% base[, IID]][sample(.N, num.related)]
        unique <-
            pheno[!(IID %in% base[, IID] |
                        IID %in% related[, ID2] |
                        IID %in% related[, ID1])][sample(.N, num.unique)]
        target <- rbind(related.target, unique)
        related.base <- related[ID2 %in% related.target[,IID]]
        expected <- base[!IID %in% target[,IID] & !IID %in% related.base[,ID1]]
    } else{
        # only need to handle population information when simulating non-related QT phenotypes
        num.overlap <- ceiling(${targetSize} * ${overlap})
        if(${has_pop}){
            pheno[,InBase := 0]
            pheno[IID %in% base[,IID], InBase := 1]
            pheno <- merge(pheno, fread("${population}"))
            # Pre-calculate the ratio so that the code can be easier to read downstream
            pheno[,Ratio:= .N/nrow(pheno), by=Group]
            base.data <- pheno[InBase == 1]
            base.data[,selected := sample(c(    rep(0, .N - floor(unique(Ratio) * num.overlap)),
                                                rep(1, floor(unique(Ratio) * num.overlap)))), by = Group]
            target.data <- pheno[InBase != 1] %>%
                .[, selected := sample(c(   rep(0, .N - floor((${targetSize} - num.overlap) * unique(Ratio))),
                                            rep(1, floor((${targetSize} - num.overlap) * unique(Ratio))))), by = Group]
            target <- rbind(target.data[selected == 1], base.data[selected == 1])
            expected <- base.data[selected == 0]
        }
        else{
            num.unique <-${targetSize} - num.overlap
            overlapped <-
                pheno[IID %in% base[, IID]][sample(.N, num.overlap)]
            unique <- pheno[!IID %in% base[, IID]][sample(.N, num.unique)]
            target <- rbind(overlapped, unique)
            expected <- base[!IID %in% target[,IID]]
        }
    }
    fwrite(target[, c("FID", "IID")], "${perm}-${herit}-${related}-${ascertain}.target", 
        sep = "\\t")
    fwrite(expected[,c("FID", "IID")], "${perm}-${herit}-${related}-${ascertain}.expected", 
        sep = "\\t")
    """
}


process random_population{
    label 'short'
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
    output:
        path("population.info")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    fread("${fam}") %>%
        setnames(., c("V1", "V2", "V3"), c("FID", "IID", "Group")) %>%
        .[, Group:= sample(c("EUR", "AFR"), .N, replace=T)] %>%
        .[, c("FID", "IID","Group")] %>%
        fwrite("population.info")
    """
}

process define_population{
    label 'short'
    publishDir "population", mode: 'copy', overwrite: true, pattern: "population*"
    publishDir "plots", mode: 'copy', overwrite: true, pattern: "*png"
    input:
        tuple  path(covar),
                val(kmean),
                val(seed)
    output:
        path "population.group.csv", emit: pop
        tuple   path("pca.png"),
                path("pca-select.png"), emit: figure
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(ggplot2)
    library(magrittr)
    # avoid using forcats as our current image does not contain it (and it avoids tiddyverse)
    cov <- fread("${covar}")
    # Set the seed for the kmean clustering
    if("${seed}"!="false"){
        set.seed("${seed}")
    }
    pc1k<-kmeans(cov[,PC1], ${kmean})
    pc2k<-kmeans(cov[,PC2], ${kmean})
    cov <- cov[,clusters:=as.factor(paste(pc1k\$cluster,pc2k\$cluster,sep="."))] %>%
        .[,c("FID", "IID", "clusters", "PC1","PC2")]
    # Plot PCA plot
    color <- c(
        "#9999FF",
        "#6699CC",
        "#99CCFF",
        "#CCCCFF",
        "#FFCCFF",
        "#FF99FF",
        "#FF99CC",
        "#CC99CC",
        "#996699",
        "#FF3399",
        "#FF6666",
        "#FF3333",
        "#00CCCC",
        "#336666",
        "#009999",
        "#003333",
        "#3366CC",
        "#0099CC",
        "#9900FF",
        "#FF0033",
        "#CC3300",
        "#666600",
        "#CCCC00",
        "#669900",
        "#CCCC66" )

    g <- ggplot(cov, aes(x=PC1,y=PC2, color=clusters))+\
            geom_point()+\
            theme_classic()+\
            scale_color_manual(values = color)+\
            theme(  legend.text = element_text(size=14),
                    legend.title = element_text(size=16, face="bold"),
                    axis.title = element_text(size=16, face="bold"),
                    axis.text = element_text(size=14))
    ggsave("pca.png", plot=g, height=7,width=7)

    max.cluster <- names(which.max(table(cov[,clusters])))
    # Assume the largest cluster is the EUR cluster
    # And 
    cov[,Group := "NA"]
    cov[clusters==max.cluster, Group:= "EUR"]
    # Now get the mean and sd of PC1 & PC2 for the EUR cluster
    region <- cov[Group=="EUR", .(PC1m = mean(PC1), PC1sd = sd(PC1), PC2m = mean(PC2), PC2sd = sd(PC2))]
    # Define non-european sample as samples 6 SD away from the mean on both side
    cov[    (PC1 > region[,PC1m] + region[,PC1sd] * 10 | PC1 < region[,PC1m] - region[,PC1sd] * 10) &
            (PC2 > region[,PC2m] + region[,PC2sd] * 10 | PC2 < region[,PC2m] - region[,PC2sd] * 10), 
            Group := "NotEUR"]
    g <- ggplot(cov, aes(x=PC1,y=PC2, color=Group))+\
            geom_point()+\
            theme_classic()+\
            scale_color_manual(values = color)+\
            theme(  legend.text = element_text(size=14),
                    legend.title = element_text(size=16, face="bold"),
                    axis.title = element_text(size=16, face="bold"),
                    axis.text = element_text(size=14))
    ggsave("pca-select.png", plot=g, height=7,width=7)
    fwrite(cov[!is.na(Group) & Group != "NA",c("FID", "IID", "Group")], "population.group.csv", sep="\\t", na="NA", quote=F)
    """
}

process extract_phenotype_from_sql{
    label 'normal'
    input:
        tuple   val(fieldID),
                path(db)
    output:
        tuple   val(fieldID),
                path("${fieldID}.csv") optional true
    script:
    """
    echo "
    .mode csv
    .header on 
    CREATE  TEMP TABLE pheno_code
    AS
    SELECT  cm.value AS value,                 
            cm.meaning AS meaning                 
    FROM    code cm                               
    JOIN    data_meta dm ON dm.code_id=cm.code_id  
    WHERE   dm.field_id=${fieldID};                   

    .output ${fieldID}.csv
    SELECT  s.sample_id AS FID,
            s.sample_id AS IID, 
            age.pheno AS Age,
            sex.pheno AS Sex,
            centre.pheno AS Centre,
            fasting.pheno AS Fasting,
            dilution.pheno AS Dilution,
            COALESCE(
               pheno_code.meaning, 
               trait.pheno) AS Pheno,
            MAX(
                CASE WHEN med.instance = 0 THEN 
                    CASE
                    WHEN
                        med.pheno in (1141146234, 1141192414, 1140910632,
                                        1140888594, 1140864592, 1141146138,
                                        1140861970, 1140888648, 1141192410,
                                        1141188146, 1140861958, 1140881748,
                                        1141200040, 1140861922)
                    THEN 1
                    ELSE 0
                    END
                END
            ) AS Statin
    FROM    f${fieldID} trait 
            LEFT JOIN   pheno_code ON 
                        pheno_code.value=trait.pheno 
            LEFT JOIN   Participant s  ON
                        s.withdrawn = 0 AND
                        s.sample_id = trait.sample_id
            JOIN        f20003 med ON 
                        s.sample_id=med.sample_id 
            LEFT JOIN   f31 sex ON
                        s.sample_id=sex.sample_id 
                        AND sex.instance = 0
            LEFT JOIN   f21003 age ON 
                        s.sample_id=age.sample_id 
                        AND age.instance = 0
            LEFT JOIN   f54 centre ON 
                        s.sample_id=centre.sample_id 
                        AND centre.instance = 0
            LEFT JOIN   f74 fasting ON
                        s.sample_id=fasting.sample_id
                        AND fasting.instance = 0
            LEFT JOIN   f30897 dilution ON
                        s.sample_id=dilution.sample_id
                        AND dilution.instance = 0
    WHERE trait.instance = (SELECT min(instance)  FROM f${fieldID})
    GROUP BY trait.sample_id;
    .quit
    " > sql;
    sqlite3 ${db} < sql  || echo "skip"
    line=`wc -l ${fieldID}.csv | cut -f 1 -d " "`
    if [[ \${line} -eq 0 ]];
    then
        rm ${fieldID}.csv
    fi
    """
}

process residualize_phenotype{
     label 'normal'
    publishDir "result/pheno", mode: 'symlink'
    input:
        tuple   val(fieldID),
                path(pheno),
                path(withdrawn),
                path(fam),
                path(cov)
    output:
        tuple   val(fieldID),
                path("${fieldID}.pheno"), emit: pheno
        path    "${fieldID}.size", emit: sampleSize
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    withdrawn <- fread("${withdrawn}", header = F)
    fam <- fread("${fam}") %>%
        .[!V2 %in% withdrawn[, V1]]
    cov <- fread("${cov}") %>%
        .[IID %in% fam[, V2]]
    pheno <- fread("${pheno}") %>%
        .[, Pheno := as.numeric(as.character(Pheno))]
    resid.form <- paste("PC", 1:40, sep = "", collapse = "+") %>%
        paste0("Pheno~Batch+Centre+Sex+Age+", .)
    if("${fieldID}" == "30780"){
        # For LDL, remove any sample on Statin
        pheno <- pheno[Statin == 0][,-c("Statin")]
        resid.form <- paste(resid.form, "Fasting+Dilution", sep="+")
    }else{
        # Ignore statin use, dilution and fasting
        pheno <- pheno[, -c("Statin", "Fasting", "Dilution")]
    }
    pheno <- pheno %>%
        merge(., cov) %>%
        na.omit %>%
        # Pasting a in front make sure plink will treat it as a factor
        .[, Centre := paste("a", Centre,sep="")] %>%
        .[, Batch := as.factor(Batch)] %>%
        na.omit %>%
        .[, Pheno := resid.form %>%
                    as.formula %>%
                    lm(., data = .SD) %>%
                    resid] %>%
        .[, c("FID", "IID", "Pheno")]
    
    fwrite(pheno, "${fieldID}.pheno", sep = "\\t")
    fwrite(data.table(FieldID="${fieldID}", SampleSize=nrow(pheno)), "${fieldID}.size")
    """
}

process glgc_ldl_protocol{
     label 'normal'
    publishDir "result/pheno", mode: 'symlink'
    input:
        tuple   val(fieldID),
                path(pheno),
                path(withdrawn),
                path(fam),
                path(cov)
    output:
        tuple   val(fieldID),
                path("${fieldID}.pheno"), emit: pheno
        path    "${fieldID}.size", emit: sampleSize
    script:
    """
    #!/usr/bin/env Rscript
    # Follow GLGC protocol https://protocolexchange.researchsquare.com/article/pex-1687/v1
    library(magrittr)
    library(data.table)
    rint <- function(x){
        n <- length(x)
        return(qnorm((rank(x)-0.5)/n))
    }
    withdrawn <- fread("${withdrawn}", header = F)
    fam <- fread("${fam}") %>%
        .[!V2 %in% withdrawn[, V1]]
    cov <- fread("${cov}") %>%
        .[IID %in% fam[, V2]]
    pheno <- fread("${pheno}") %>%
        .[, Pheno := as.numeric(as.character(Pheno))] %>%
        .[, Age2 := Age^2]
    resid.form <- paste("PC", 1:40, sep = "", collapse = "+") %>%
        paste0("Pheno~Batch+Centre+Age+Age2+", .)
    if("${fieldID}" == "30780"){
        # For LDL, remove any sample on Statin
        pheno <- pheno[!is.na(Statin) & Statin != 0, Pheno := Pheno/0.7] %>%
            .[!is.na(Pheno)& !is.na(Fasting) & ! is.na(Dilution)]
        pheno <- pheno[,-c("Statin")]
        resid.form <- paste(resid.form, "Fasting+Dilution", sep="+")
    }else{
        stop("This is for LDL only")
    }
    pheno <- pheno %>%
        merge(., cov) %>%
        na.omit %>%
        # Pasting a in front make sure plink will treat it as a factor
        .[, Centre := paste("a", Centre,sep="")] %>%
        .[, Batch := as.factor(Batch)] %>%
        na.omit %>%
        .[, Pheno := resid.form %>%
                    as.formula %>%
                    lm(., data = .SD) %>%
                    resid, by = Sex] %>%
        .[, Pheno := rint(Pheno), by=Sex] %>%
        .[, c("FID", "IID", "Pheno")]
    
    fwrite(pheno, "${fieldID}.pheno", sep = "\\t")
    fwrite(data.table(FieldID="${fieldID}", SampleSize=nrow(pheno)), "${fieldID}.size")
    """
}
process select_base_samples_by_ratio{
    label 'tiny'
    input:
        tuple   val(meta),
                path(relatedInfo),
                path(pheno)
    output:
        tuple   val(updatedMeta),
                path(relatedInfo),
                path(pheno),
                path("${type}-${perm}.id")
    script:
    updatedMeta = meta.clone()
    updatedMeta["sample"] = "Base"
    type = get_meta_value(meta, "type", "QT")
    seed = get_meta_value(meta, "seed", 1234)
    perm = get_meta_value(meta, "perm", 0)
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    set.seed(${seed})
    # Read in all phenotype
    pheno <- fread("${pheno}")
    # One third of all valid sample as base
    pheno <- pheno[sample(.N, .N * 2 / 3)]
    fwrite(pheno[, c("IID", "FID")],
           "${type}-${perm}.id",
           sep = "\\t")
    """
}

process select_target_samples_by_ratio{
    label 'tiny'
    input:
        tuple   val(meta),
                path(relatedInfo),
                path(pheno),
                path(baseSample)
    output:
        tuple   val(metaTarget),
                path(relatedInfo),
                path(pheno),
                path("${type}-${perm}-${ascertain}.target"), emit: target
        tuple   val(metaExpected),
                path(relatedInfo),
                path(pheno),
                path("${type}-${perm}-${ascertain}.expected"), emit: expected
        path("${trait}-${perm}-${ascertain}-${overlap}.size"), emit: sampleSize
    script:
    metaTarget = meta.clone()
    metaTarget["sample"] = "Target"
    metaExpected = meta.clone()
    metaExpected["sample"] = "Expected"
    perm = get_meta_value(meta, "perm", 0)
    related = get_meta_value(meta, "related", false)
    ascertain = get_meta_value(meta, "ascertain", "Random")
    overlap = get_meta_value(meta, "overlap", 0)
    type = get_meta_value(meta, "type", "QT")
    seed = get_meta_value(meta, "seed", 1234)
    trait = get_meta_value(meta, "trait", "QT")
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    set.seed(${seed})
    # Read in all phenotype
    pheno <- fread("${pheno}")
    base <- fread("${baseSample}")
    unique <- NULL
    target <- NULL
    expected <- NULL
    targetSize <- floor(nrow(pheno) - nrow(base))
    num.overlap <- ceiling(targetSize * ${overlap})
    num.unique <-targetSize- num.overlap
    overlapped <-
        pheno[IID %in% base[, IID]][sample(.N, num.overlap)]
    unique <- pheno[!IID %in% base[, IID]][sample(.N, num.unique)]
    target <- rbind(overlapped, unique)
    expected <- base[!IID %in% target[,IID]]
    targetSize <- nrow(target)
    fwrite(target[, c("FID", "IID")], "${type}-${perm}-${ascertain}.target", 
        sep = "\\t")
    fwrite(expected, "${type}-${perm}-${ascertain}.expected", 
        sep = "\\t")
    data.table(Perm = ${perm}, type="${type}", Overlap=${overlap}, Trait = "${trait}", BaseSize = nrow(base), TargetSize = nrow(target)) %>%
        fwrite(., "${trait}-${perm}-${ascertain}-${overlap}.size")
    """
}
