#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'

// Also, make sure seed is provided, otherwise we can't do -resume
timestamp='2021-08-06'
if(params.version) {
    System.out.println("")
    System.out.println("Calculate LD score for UK biobank - Version: $version ($timestamp)")
    exit 1
}

if(params.help){
    System.out.println("")
    System.out.println("Calculate LD score for UK biobank - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 1_prepare_ldscore.nf [options]")
	System.out.println("Mandatory arguments:")
    System.out.println("    --ref         Reference genotype file prefix (1000 Genome)")
    System.out.println("    --pop         Population information for reference data")
    System.out.println("    --printsnp    QCed UK Biobank SNPs")
    System.out.println("    --help        Display this help messages")
} 

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   genotype_filtering   }   from './modules/basic_genetic_analyses'
include {   calculate_ldscore   }   from './modules/calculate_ldscore'
////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}

////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////
// For printsnp, try to use the QCed SNP list from UK Biobank so that we maximize overlap in downstream analyses
printsnp = Channel.fromPath("${params.printsnp}")
chromosome = Channel.value(1..22).flatten()
reference = chromosome \
    | map{ a -> [   get_chr(a, "${params.ref}"), 
                    fileExists(file(gen_file(a, "${params.ref}.bed"))),
                    fileExists(file(gen_file(a, "${params.ref}.bim"))),
                    fileExists(file(gen_file(a, "${params.ref}.fam")))]}
pop = Channel.fromPath("${params.pop}")

workflow{
    // 1. Filter out EUR samples as only EUR samples were used for simulation
    reference \
        | combine(pop) \
        | combine(printsnp) \
        | genotype_filtering \
    /* 2. Then calculate the LD score for both the baseline
          which contains all SNPs in EUR, and weight, which
          contains only SNPs in the UK biobank data */
        | mix \
        | combine(printsnp) \
        | combine(["move"]) \
        | calculate_ldscore
}