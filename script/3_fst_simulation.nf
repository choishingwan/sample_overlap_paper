#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'

// Also, make sure seed is provided, otherwise we can't do -resume
timestamp='2020-11-09'
if(params.version) {
    System.out.println("")
    System.out.println("Run sample overlap simulation - Version: $version ($timestamp)")
    exit 1
}

params.seed = 1234
params.develop = true
if(params.help){
    System.out.println("")
    System.out.println("Run sample overlap simulation - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 3_fst_simulation.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --fin         1000 Genome FIN recomnination rate ")
    System.out.println("    --yri         1000 Genome YRI recombination rate")
    System.out.println("    --genome      1000 Genome lagend and hap folder")
    System.out.println("    --hapmap      HapMap SNPs")
    System.out.println("    --perm        Total permutation")
    System.out.println("    --effBase     Effective sample size for base. Default ${params.effBase}")
    System.out.println("    --effTarget   Effective sample size for target. Default ${params.effTarget}")
    System.out.println("    --seed        Seed for random number generation")
    System.out.println("    --help        Display this help messages")
} 


////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def get_prefix(str){
    if(str.split('-')[0] == "NA"){
        error("\n\n----------\n$str cannot be tokenized\n")
    }else 
        return str.split('-')[0];
}
def gen_file(chr, input){
    return input.replaceAll("#",chr.toString())
}

def get_chr(a, input){
    if(input.contains("#")){
        return a
    }
    else {
        return 0
    }
}
////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   removeElements
            addMeta
            removeMeta
            combine_map  }   from './modules/handle_meta'
include {   calculate_ldscore   }   from './modules/calculate_ldscore'
include {   extract_valid_samples
            calculate_xbeta
            generate_phenotype
            select_base_samples
            select_target_samples
            random_population  }   from './modules/phenotype_simulation'
include {   run_gwas    }   from './modules/basic_genetic_analyses'
include {   extract_haplotype
            run_hapgen
            calculate_pca
            merge_genotype
            trim_geno_sample_for_ldsc   }   from './modules/genotype_simulation'
include {   run_erasor
            run_prsice
            update_prsice
            update_prsice as update_prsice_rand
            modify_erasor_meta
            combine_files
            combine_files as combine_prsice
            combine_files as combine_rand_prsice
            combine_tmp_files
            combine_tmp_files as combine_rand_tmp_files
            process_overlap_results
            process_overlap_results as process_overlap_rand_results   }   from './modules/polygenic_analyses'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////

chromosome = Channel.of(1..22)
reference = chromosome \
    | combine(Channel.of("${params.genome}/")) \
    | map{ a -> [   a[0],   // chromosome
                    fileExists(file(a[1]+"/1000GP_Phase3_chr"+a[0]+".hap.gz")),
                    fileExists(file(a[1]+"/1000GP_Phase3_chr"+a[0]+".legend.gz")),
                    fileExists(file(a[1]+"/1000GP_Phase3.sample"))]}
hapmap = Channel.fromPath("${params.hapmap}")
fin_recombination = chromosome \
    | combine(Channel.of("FIN")) \
    | combine(Channel.fromPath("${params.recom}/FIN/*")) \
    | filter{ a -> a[2] =~ /-${a[0]}-/} 
yri_recombination = chromosome \
    | combine(Channel.of("YRI")) \
    | combine(Channel.fromPath("${params.recom}/YRI/*")) \
    | filter{ a -> a[2] =~ /-${a[0]}-/} 
    
def index = 0
perm_seed = Channel
    .from( 1 .. 100000 )
    .randomSample( params.perm, params.seed )
    .map{ a-> [index++, a] }

workflow{
    genotype_simulation()
    
    phenotype_simulation(
        genotype_simulation.out.geno,
        genotype_simulation.out.pop
        )
    generate_study_samples(
        phenotype_simulation.out.pop,
        phenotype_simulation.out.pheno
    )
    generate_summary_statistics(
        generate_study_samples.out,
        genotype_simulation.out.geno,
        genotype_simulation.out.pcs,
        genotype_simulation.out.score
    )
    
    polygenic_score_analyses(
        generate_study_samples.out,
        generate_summary_statistics.out,
        genotype_simulation.out.geno,
        genotype_simulation.out.pcs
    )
}

workflow genotype_simulation{
    main:
        // 1. First, extract population specific haplotypes from the 1000 Genome
        //    this allow us to simulate distinct populations
        recombination = yri_recombination \
            | mix(fin_recombination) 
        reference \
            | combine(hapmap) \
            | extract_haplotype \
            | mix \
            | combine(recombination, by: [0, 1]) \
            | combine(Channel.of("${params.seed}")) \
        /* 2. Simulate one set of genotype. Genotype simulation is extremely expensive
              in both computation time, memory and storage space requirement. It can
              take a day to get the PCA calculated */
            | run_hapgen \
            | map{ a -> removeElements(x: a, idx: [0])} \
            | collect \
            | merge_genotype
        merge_genotype.out.geno \
            | calculate_pca
        ldscore = chromosome \
            | combine(merge_genotype.out.geno) \
            | trim_geno_sample_for_ldsc \
            | map{ a -> [
                a[0],   // chromosome number
                [],     // name
                a[1],   // bed
                a[2],   // bim
                a[3],   // fam
                [],      // stub for print SNP
                "symlink"
            ]} \
            | calculate_ldscore \
            | groupTuple \
            | map{  a ->    def name = a[0]
                            def scores = []
                            for(def i =0; i < a[1].size(); ++i ){
                                scores << a[1][i]
                            }
                            for(def i =0; i < a[2].size(); ++i ){
                                scores << a[2][i]
                            }
                            for(def i =0; i < a[3].size(); ++i ){
                                scores << a[3][i]
                            }
                            return([name+"-", scores])
                            }
    emit:
        geno = merge_genotype.out.geno
        pop = merge_genotype.out.population
        pcs = calculate_pca.out
        score = ldscore
}

workflow phenotype_simulation{
    take: geno
    take: pop
    main: 
        // 1. Calculate the Xbeta given there are 10,000 causal variants
        //    Effect size were simulated with normal distribution and 
        //    we standardize the genotypes using all samples
        numCausal = Channel.of(10000)
        numCausal \
            | combine(perm_seed) \
            | map{ a -> def meta = [:]
                        meta["numCausal"] = a[0]
                        meta["perm"] = a[1]
                        meta["seed"] = a[2]
                        meta["Stability"] = 1
                        return([ meta ])} \
            | combine(geno) \
            | map{ a -> [
                a[0],   // meta
                [],     // samples to extract
                [],     // related information
                a[1],   // bed
                a[2],   // bim 
                a[3]   // fam
            ]} \
            | combine(hapmap) \
            | calculate_xbeta
        heritability = Channel.of(0, 0.1, 0.5)
        sigmaS = Channel.of(0, 0.3, 0.9)
        // also do random population
        rand_pop = random_population(geno) \
            | map{ a -> ["Random", a]}
        population = pop \
            | map{ a -> ["Real", a]} \
            | mix(rand_pop)
        // Won't simulate Case Control and Related samples as that will be too complicated
        calculate_xbeta.out \
            | combine(heritability) \
            | combine(sigmaS) \
            | combine(population) \
            | map{ a -> def meta = a[0].clone()
                        meta["herit"] = a[3]
                        meta["sigmaS"] = a[4]
                        meta["PopType"] = a[5]
                        // now do conditional return
                        def result = [  meta,   // meta information
                                        a[1],   // related information
                                        a[2],   // xbeta
                                        a[6]]   // Population
                        if(meta["herit"] + meta["sigmaS"] < 1){
                            return(result)
                        }
            } \
            | generate_phenotype
    emit: 
        pheno = generate_phenotype.out
        pop = population
}

workflow generate_study_samples{
    take: pop
    take: pheno
    main:
        // 1. First, randomly select N samples as base. For relatedness analysis, always
        //    include most if not all samples with relatives in the base. 
        //    for related samples, we don't bother with varying the base sample size;
        //    whereas for CC, we don't want to include too much base sample size as that'd
        //    be difficult to have enough samples to achieve the same effective sample size


        baseSize = Channel.of(250000, 120000)
        pheno \
            | combine(baseSize) \
            | combine(pop) \
            | map{ a -> def meta = a[0].clone()
                    meta["baseSize"] =  a[3]
                    def baseData = [    meta,   // meta information
                                        a[1],   // related information
                                        a[2],   // phenotype
                                        a[5]]   // pop file
                    // only iterate the base sample size when heritability = 0 or 0.1    
                    if(meta.baseSize == 250000 || meta.herit == 0 || meta.herit == 0.1){
                        if(meta.PopType == a[4]){
                            return(baseData)
                        }
                    }
                } \
            | select_base_samples

        overlap = Channel.of(0, 0.1, 0.5, 1) 
        targetSize = Channel.of(5000, 10000)
        select_base_samples.out \
            | combine(overlap) \
            | combine(targetSize) \
            | combine(pop) \
            | map{ a -> def meta = a[0].clone()
                        meta["overlap"]  = a[4]
                        meta["targetSize"] = a[5]
                        def result = [  meta,   // meta information
                                        a[1],   // related pair
                                        a[2],   // phenotype
                                        a[3],   // base ID
                                        a[7]]   // Population
                        // only alter base size when target size is 5000 and vis versa
                        if( meta.herit == 0 || meta.herit==0.1 || 
                            meta.baseSize == 250000 || 
                            meta.targetSize == 5000)
                            if(meta.PopType == a[6]){
                                return(result)
                            }
                        
                } \
            | unique \
            | select_target_samples

        /*
            When overlap is 0, expected == target, so we don't want to repeat the analysis for that as it can be expensive
        */
        samples = select_target_samples.out \
            | mix \
            | mix(select_base_samples.out)  \
            | filter{ a -> a[0].sample != "Expected" || a[0].overlap != 0 } 
    emit:
        samples
}


workflow generate_summary_statistics{
    take: samples
    take: geno
    take: pcs
    take: score
    main:
        // 1. Perform GWAS analysis on base and target samples
        //    Don't need to do it on the expected samples
        // Must include PCs, otherwise, LDSC will break (Huge negative h2 when sigmaS is high)
       samples \
            | filter{ a -> a[0].perm   < params.maxPerm } \
            | combine(Channel.of(["PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,PC11,PC12,PC13,PC14,PC15"]))  \
            | map{ a ->  addMeta(x: a, meta: [cov: a[4]])} \
            | map{ a -> removeElements(x: a, idx: [4])} \
            | combine(geno) \
            | map{ a -> [
                a[0],   // meta
                a[1],   // related
                a[2],   // pheno
                a[3],   // ID
                a[4],   // bed
                a[5],   // bim
                a[6],   // fam
                []  // stub
            ]} \
            | combine(pcs) \
            | run_gwas
        // 2. Extract the base GWAS
        base = run_gwas.out \
            | filter{ a -> a[0].sample == "Base"}
        target = run_gwas.out \
            | filter{ a -> a[0].sample == "Target"}
        expected = run_gwas.out \
            | filter{ a -> a[0].sample == "Expected"}
        // 3. Run EraSOR to obtain the adjusted GWAS
        combine_map(x: target, 
                    y: base, 
                    by: 0,
                    column: ["perm", "PopType", "herit","numCausal", "sigmaS", "cov", "baseSize"]) \
            | combine(score) \
            | map{ a -> [
                a[0],   // meta 
                a[1],   // target GWAS
                a[2],   // base GWAS
                a[3],   // base score name
                a[4],   // base score files
                a[3],   // weight (use base score in simulation)
                []      // stub
            ]} \
            | combine(Channel.fromPath("${params.erasor}")) \
            | run_erasor
/* 
        metaResult = run_erasor.out.info \
            | modify_erasor_meta \
            | map{ a -> [   a[0].perm, 
                            a[1]]} \
            | groupTuple \
            | combine_tmp_files \
            | collect 
*/
        gwas = run_erasor.out.gwas \
            | mix(base) \
            | mix(expected)
  //      combine_files("fst-meta.csv","result", metaResult)
    emit: 
        gwas
}

workflow polygenic_score_analyses{
    take: samples
    take: gwas
    take: geno
    take: pcs
    main:
        // 1. First, remove all base samples as we won't be using them for PRSice
        //    Also, need to rearrange the sample type to the back
        pheno = samples \
            | filter{ a -> a[0].sample == "Target"}
        baseGWAS = gwas \
            | filter{ a -> a[0].sample == "Base"}
        base = combine_map(x: baseGWAS, y: pheno, by:0, column: ["perm",  "PopType", "herit", "numCausal", "sigmaS", "baseSize"])
        targetGWAS = gwas \
            | filter{ a -> a[0].sample != "Base"}
        withPC = combine_map(x: targetGWAS, y: pheno, by:0, column: ["perm",  "PopType", "herit", "numCausal", "overlap", "targetSize", "baseSize",  "sigmaS"]) \
            | mix(base) \
            | combine(geno) \
            | combine(pcs) \
            | run_prsice
        sim_pop = run_prsice.out \
            | filter{ a -> a[0].PopType == "Real"} \
            | update_prsice \
            | groupTuple \
            | combine_tmp_files \
            | collect
        rand_pop = run_prsice.out \
            | filter{ a -> a[0].PopType != "Real"} \
            | update_prsice_rand \
            | groupTuple \
            | combine_rand_tmp_files \
            | collect
                
        combine_prsice("fst-prs.csv", "result", sim_pop)  \
            | combine(Channel.of("FST.Sim")) \
            | combine(Channel.of("hapgen-prs.csv")) \
            | combine(Channel.of("result")) \
            | process_overlap_results
        combine_rand_prsice("fst-rand-prs.csv", "result", rand_pop)  \
            | combine(Channel.of("FST.Rand.Sim")) \
            | combine(Channel.of("hapgen-rand-prs.csv")) \
            | combine(Channel.of("result")) \
            | process_overlap_rand_results
}
