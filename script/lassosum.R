#!/usr/bin/env Rscript
library(lassosum)
library(data.table)
library(methods)
library(magrittr)
library(parallel)
library(optparse)
options(stringsAsFactors = FALSE)

# Standardize the parameter so that it is similar to PRSice
option_list <- list(
  make_option(
    c(
      "-t",
      "--target",
      type = "character",
      default = NULL,
      help = "Binary plink file prefix. Use # to denote chromosome number"
    )
  ),
  make_option(
    c(
      "--ld",
      type = "character",
      default = NULL,
      help = "Binary plink file prefix for LD reference. Use # to denote chromosome number"
    )
  ),
  make_option(c("-b", "--base"), type = "character", help = "Summary statistic file"),
  make_option(
    c("--snp"),
    type = "character",
    default = "ID",
    help = "column name for SNP ID in sumstat file"
  ),
  make_option(
    c("--effective"),
    type = "character",
    default = "A1",
    help = "column name for the effective allele in sumstat file"
  ),
  make_option(
    c("--reference"),
    type = "character",
    default = "AX",
    help = "column name for the non-effective allele in sumstat file"
  ),
  make_option(
    c("--chr"),
    type = "character",
    default = NULL,
    help = "column name for CHR in sumstat file"
  ),
  make_option(
    c("--Ncol"),
    type = "character",
    default = NULL,
    help = "column name for Sample Size in sumstat file"
  ),
  make_option(
    c("--N"),
    type = "numeric",
    default = NULL,
    help = "Number of sample in sumstat file"
  ),
  make_option(
    c("--bp"),
    type = "character",
    default = NULL,
    help = "column name for BP in sumstat file"
  ),
  make_option(
    c("--pvalue"),
    type = "character",
    default = "P",
    help = "column name for the P-value in sumstat file"
  ),
  make_option(
    c("--stat"),
    type = "character",
    default = "BETA",
    help = "column name for the effect size in sumstat file"
  ),
  make_option(
    c("--or"),
    action = "store_true",
    default = FALSE,
    help = "Indicate the effect size is OR"
  ),
  make_option(
    c("--score"),
    action = "store_true",
    default = FALSE,
    help = "Print best score for all samples in the phenotype file"
  ),
  make_option(
    c("--all"),
    action = "store_true",
    default = FALSE,
    help = "Print all score for all samples in the phenotype file"
  ),
  make_option(c("--ld.keep"), type = "character", help = "Sample used for LD calculation. Assume second column is the IID"),
  make_option(
    c("--out"),
    type = "character",
    default = "lassosum",
    help = "Output prefix"
  ),
  make_option(
    c("-n", "--thread"),
    type = "numeric",
    default = 1,
    help = "Number of thread used"
  ),
  make_option(c("-p", "--pheno"), type = "character", help = "Phenotype file"),
  make_option(c("--name"), type = "character", help = "Phenotype name"),
  make_option(
    c("--covFile"),
    type = "character",
    default = NULL,
    help = "Covariate file"
  ),
  make_option(c("--covar"), default = NULL, type = "character", help = "Covariate for regression"),
  make_option(c("--factor"), default = NULL, type = "character", help = "Factor covariates"),
  make_option(
    c("--pop"),
    type = "character",
    default = "EUR",
    help = "Population. Support ASN, EUR and AFR"
  ),
  make_option(
    c("--train"),
    type = "character",
    default = NULL,
    help = "Subset of --pheno used for training. Will use all samples in --pheno if not provided"
  ),
  make_option(
    c("--validate"),
    type = "character",
    default = NULL,
    help = "Subset of --pheno use for validation. Will not perform validation if this is not provided"
  )
)

opt_parser <- OptionParser(option_list = option_list)

opt <- parse_args(opt_parser)
cl <- makeCluster(opt$thread)
if (is.null(opt$covFile) &
    !is.null(opt$covar)) {
  stop("Error: Must provide covariate file to include covariates")
}

cov.factor <- strsplit(as.character(opt$factor), split = ",")

covar <- strsplit(as.character(opt$covar), split = ",")[[1]]
if (!all(cov.factor %in% covar)) {
  stop("Error: All factor covariates must be included in --covar")
}

sum.stat <- opt$base
bfile <- opt$target
ld <- opt$ld

if (is.null(ld))
  ld <- bfile
pop <- opt$pop
if (pop == "EAS") {
  pop <- "ASN"
}
ld.file <- paste0(pop, ".hg19")
ss <- fread(sum.stat)
setnames(ss, opt$pvalue, "P")

if (!is.null(opt$chr)) {
  setnames(ss, opt$chr, "CHR")
}
if (!is.null(opt$bp)) {
  setnames(ss, opt$bp, "BP")
}
setnames(ss, opt$snp, "SNP")
setnames(ss, opt$effective, "A1")
setnames(ss, opt$reference, "AX")
if (!is.null(opt$Ncol)) {
  setnames(ss, opt$Ncol, "OBS_CT")
}
setnames(ss, opt$stat, "BETA")
ss <- ss[, P := as.numeric(P)]
ss <- ss[P != 0 & !is.na(P)]
ss <- ss[, BETA := as.numeric(BETA)]
if (!is.null(opt$Ncol)) {
  ss <- ss[, OBS_CT := as.numeric(OBS_CT)]
}
ld.fam <- NULL
if (is.null(opt$ld.keep)) {
  if (!is.null(opt$keep)) {
    opt$ld.keep <- opt$keep
    ld.fam <- fread(opt$ld.keep) %>%
      .[, c("FID", "IID")] %>%
      as.data.frame
  } else{
    ld.fam <- fread(paste0(bfile, ".fam")) %>%
      .[, c("V1", "V2")] %>%
      setnames(., c("V1", "V2"), c("FID", "IID")) %>%
      as.data.frame
  }
} else{
  ld.fam <- fread(opt$ld.keep) %>%
    .[, c("FID", "IID")] %>%
    as.data.frame
}

if (nrow(ld.fam) > 5000) {
  ld.fam <- ld.fam %>%
    as.data.table %>%
    .[sample(.N, 5000)] %>%
    as.data.frame
}

cor <- p2cor(p = ss$P,
             n = if (!is.null(opt$Ncol)) {
               ss$OBS_CT
             } else{
               opt$N
             },
             sign = ss$BETA)

out <- lassosum.pipeline(
  cor = cor,
  chr = if (is.null(opt$chr)) {
    NULL
  } else{
    ss$CHR
  },
  pos = if (is.null(opt$bp)) {
    NULL
  } else{
    ss$BP
  },
  snp = if (is.null(opt$snp)) {
    NULL
  } else{
    ss$SNP
  },
  A1 = ss$A1,
  A2 = ss$AX,
  ref.bfile = ld,
  test.bfile = bfile,
  LDblocks = ld.file,
  keep.ref = ld.fam,
  cluster = cl
)

fam <-  fread(paste0(bfile, ".fam"))
target.pheno <-
  fread(opt$pheno)[, c("FID", "IID", opt$name), with = F] %>%
.[IID %in% fam[,V2]]
  #%>%
  #.[!is.na(get(opt$name))]
training.samples <- if (is.null(opt$train)) {
  target.pheno[, c("FID", "IID")]
} else{
  fread(opt$train)[, c("FID", "IID")]
}
training.pheno <- target.pheno[IID %in% training.samples[, IID]] %>%
  as.data.frame
cov.matrix <- NULL
if (!is.null(opt$covFile)) {
  cov.matrix <- fread(opt$covFile)[, c("FID", "IID", covar), with = F] %>%
    .[IID %in% fam[,V2]]
  if (length(cov.factor) > 0) {
    cov.matrix[, (cov.factor) := lapply(.SD, factor), .SDcols = cov.factor]
  }
  cov.matrix <- cov.matrix[IID %in% training.samples[, IID]] %>%
    as.data.frame
}

print("Start performing training")
training.res <-
  validate(
    out,
    pheno = training.pheno,
    covar = cov.matrix,
    plot = F,
    validate.function = function(x, y) {
      cor(x, y, use = "complete") ^ 2
    }
  )

training.P <-
  validate(
    out,
    pheno = training.pheno,
    covar = cov.matrix,
    plot = F,
    validate.function = function(x, y) {
      coef <- (summary(lm(y~x))$coefficients)
      if(nrow(coef) == 2){
        return(coef[2,4])
      }else{
        return(NA)
      }
    }
  )
training.p <- as.data.table(training.P$validation.table)
pvalue <- training.p[lambda == training.res$best.lambda & s == training.res$best.s, value]
v <- pseudovalidate(out)
pseudo.valid.res <- as.data.table(v$validation.table)
pseudo.r2 <- pseudo.valid.res[lambda==v$best.lambda & s==v$best.s,"value"]^2
result <-
  data.table(
    Software = "lassosum",
    PRS.R2 = training.res$best.validation.result,
    P  = pvalue
  ) %>%
  rbind(., data.table(
    Software = "lassosum.pseudo",
    PRS.R2 = as.numeric(pseudo.r2),
    P  = NA), fill=TRUE)

fwrite(result, paste(opt$out,"result", sep=".") )
