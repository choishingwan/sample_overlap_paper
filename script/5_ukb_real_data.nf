#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'

// Also, make sure seed is provided, otherwise we can't do -resume
timestamp='2021-12-17'
if(params.version) {
    System.out.println("")
    System.out.println("Run sample overlap simulation with real UK biobank phenotyeps- Version: $version ($timestamp)")
    exit 1
}

params.seed = 1234
params.develop = false
if(params.help){
    System.out.println("")
    System.out.println("Run sample overlap simulation - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 5_ukb_real_data.nf [options]")
    System.out.println("Mandatory arguments:")
    Sysmte.out.println("    --sql         UK Biobank SQLite database")
    System.out.println("    --score       Folder containing LD Scores. Should have")
    System.out.println("                  format of baseline-chr and weight-chr")
    System.out.println("    --geno        Genotype file prefix ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --drop        Samples who withdrawn consent")
    System.out.println("    --cov         Covariate file containing the PCs")
    System.out.println("    --perm        Total permutation")
    System.out.println("    --seed        Seed for random number generation")
    System.out.println("    --help        Display this help messages")
} 



////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def get_prefix(str){
    if(str.split('-')[0] == "NA"){
        error("\n\n----------\n$str cannot be tokenized\n")
    }else 
        return str.split('-')[0];
}
def gen_file(chr, input){
    return input.replaceAll("#",chr.toString())
}

def get_chr(a, input){
    if(input.contains("#")){
        return a
    }
    else {
        return 0
    }
}
////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   removeElements
            addMeta
            removeMeta
            combine_map  }   from './modules/handle_meta'


include {   extract_valid_samples
            select_base_samples
            select_target_samples
            extract_phenotype_from_sql
            residualize_phenotype  }   from './modules/phenotype_simulation'
include {   run_gwas    }   from './modules/basic_genetic_analyses'
include {   run_erasor
            run_prsice
            update_prsice
            modify_erasor_meta
            combine_files
            combine_files as combine_prsice
            combine_tmp_files
            process_real_overlap_results  }   from './modules/polygenic_analyses'

////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////
erasor = Channel.fromPath("${params.erasor}")
genotype = Channel
    .fromFilePairs("${params.geno}.{bed,bim,fam}",size:3, flat : true){ file -> file.baseName }
    .ifEmpty { error "No matching plink files" }        
    .map { a -> [fileExists(a[1]), fileExists(a[2]), fileExists(a[3])] } 
qc_fam = Channel.fromPath("${params.fam}")
cov = Channel.fromPath("${params.cov}")
snp = Channel.fromPath("${params.snp}")
sql = Channel.fromPath("${params.sql}")
withdrawn = Channel.fromPath("${params.drop}")

score = Channel.fromPath("${params.score}/*") \
    | map { a -> [ get_prefix(a.baseName)+"-", file(a)]} \
    | groupTuple()

def index = 0
perm_seed = Channel
    .from( 1 .. 100000 )
    .randomSample( params.perm, params.seed )
    .map{ a-> [index++, a] }
phenotypes = Channel.of("50" /*height*/, "21001"/*BMI*/, "30780" /*LDL*/)



workflow{
    generate_phenotype()
    generate_study_samples(
        generate_phenotype.out
    )
    
    generate_summary_statistics(
        generate_study_samples.out
    )
    polygenic_score_analyses(
        generate_study_samples.out,
        generate_summary_statistics.out
    )
}


workflow generate_phenotype(){
    phenotypes \
        | combine(sql) \
        | extract_phenotype_from_sql \
        | combine(withdrawn) \
        | combine(qc_fam) \
        | combine(cov) \
        | residualize_phenotype
    pheno = residualize_phenotype.out.sampleSize \
        | splitCsv(header: true) \
        | map{ a -> [a.FieldID, a.SampleSize] } \
        | combine(residualize_phenotype.out.pheno, by: 0)
    emit: 
    pheno = pheno
}

workflow generate_study_samples{
    take: pheno
    main:
        // 1. First, randomly select N samples as base. For relatedness analysis, always
        //    include most if not all samples with relatives in the base. 
        //    for related samples, we don't bother with varying the base sample size;
        //    whereas for CC, we don't want to include too much base sample as that'd
        //    be difficult to have enough samples to achieve the same effective sample size

        overlap = Channel.of(0, 0.05, 0.1, 0.5,0.6,0.7,0.8,0.9, 1)
        targetSize = Channel.of(5000, 10000, 50000, 999999)
        pheno \
            | combine(perm_seed) \
            | combine(cov) \
            | map{ a -> def meta =  [:]
                    meta["trait"] = a[0]
                    // sample size is a[1]
                    // phenotype is a[2]
                    meta["type"]  = "QT"
                    meta["perm"] = a[3]
                    meta["seed"] = a[4]
                    meta["sampleSize"] = a[1].toInteger()
                    // Need to account for sample size difference, where sample with phenotype might have less than
                    // 250000 samples
                    meta["baseSize"] = Math.min(Math.floor(a[1].toInteger() * 2.0 / 3.0), 250000)
                    def baseData = [    meta,   // meta information
                                        [],     // STUB
                                        a[2],   // phenotype
                                        []]     // population information (stub)
                    return(baseData)
                } \
            | select_base_samples \
            | combine(overlap) \
            | combine(targetSize) \
            | map{ a -> def meta = a[0].clone()
                        meta["overlap"]  = a[4]
                        meta["targetSize"]  = Math.min(Math.floor(meta.sampleSize / 3.0).toInteger(), a[5].toInteger())
                        def result = [  meta,   // meta information
                                        a[1],   // related pair
                                        a[2],   // phenotype
                                        a[3],   // base ID
                                        []]     // stub for phenotype
                        return(result)
                } \
            | unique \
            | select_target_samples
        samples = select_target_samples.out \
            | mix \
            | mix(select_base_samples.out)  \
            | filter{ a -> a[0].sample != "Expected" || a[0].overlap != 0 } 
    emit:
        samples
}

workflow generate_summary_statistics{
    take: samples
    main:
        // 1. Perform GWAS analysis on base and target samples
        // Covariate is NA, because our phenotypes should be residualized
        samples \
            | combine(["NA"])  \
            | map{ a ->  addMeta(x: a, meta: [cov: a[4]])} \
            | map{ a -> removeElements(x: a, idx: [4])} \
            | combine(genotype) \
            | combine(snp) \
            | combine(cov) \
            | unique \
            | run_gwas
        // 2. Extract the base GWAS
        base = run_gwas.out \
            | filter{ a -> a[0].sample == "Base"}
        target = run_gwas.out \
            | filter{ a -> a[0].sample == "Target"}
        expected = run_gwas.out \
            | filter{ a -> a[0].sample == "Expected"}
        // 3. Extract the ld score to appropriate groups
        baseline = score \
            | filter { a -> (a[0] =~ /baseline/) }
        weight = score \
            | filter { a -> (a[0] =~ /weight/) }
        // 4. Run EraSOR to obtain the adjusted GWAS
        combine_map(x: target, 
                    y: base, 
                    by: 0,
                    column: ["perm", "type", "trait",  "cov"]) \
            | combine(baseline) \
            | combine(weight) \
            | combine(erasor) \
            | run_erasor 
        metaResult = run_erasor.out.info \
            | unique \
            | modify_erasor_meta \
            | map{ a -> a[1]} \
            | collect 
        gwas = run_erasor.out.gwas \
            | mix(base) \
            | mix(expected)
      // combine_files("meta.csv","result", metaResult)
    emit: 
        gwas
}


workflow polygenic_score_analyses{
    take: samples
    take: gwas
    main:
        // 1. First, remove all base samples as we won't be using them for PRSice
        pheno = samples \
            | filter{ a -> a[0].sample == "Target"} 
        baseGWAS = gwas \
            | filter{ a -> a[0].sample == "Base"} 
        base = combine_map(x: baseGWAS, y: pheno, by:0, column: ["perm", "trait", "type", "baseSize"]) 
            
        targetGWAS = gwas \
            | filter{ a -> a[0].sample != "Base"}
        combine_map(x: targetGWAS, y: pheno, by:0, column: ["perm", "trait", "overlap", "type", "baseSize", "targetSize"]) \
            | mix(base) \
            | combine(genotype) \
            | combine(cov) \
            | unique \
            | run_prsice \
            | map{ a -> def meta = a[0].clone()
                // we will use Ascertainment to store our trait name, as such we can reuse the update_prsice process
                meta["ascertain"]  = meta.trait
                return [meta,
                        a[1]]} \
            | update_prsice \
            | groupTuple \
            | combine_tmp_files
        res = combine_tmp_files.out \
            | collect
        combine_prsice("real_prs.csv", "result", res) \
            | combine(Channel.of("UKB.Real")) \
            | combine(Channel.of("ukb-real-prs.csv")) \
            | combine(Channel.of("result")) \
            | process_real_overlap_results
}

