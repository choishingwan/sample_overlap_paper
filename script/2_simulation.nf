#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
params.runRelate=false
params.runCase=false
version='0.0.4'

// Also, make sure seed is provided, otherwise we can't do -resume
// formalize the script so that it can run the additional analyses together with the original analyses
timestamp='2023-05-04'
// definitely should be on leave by now
if(params.version) {
    System.out.println("")
    System.out.println("Run sample overlap simulation - Version: $version ($timestamp)")
    exit 1
}

params.seed = 1234
params.develop = false
if(params.help){
    System.out.println("")
    System.out.println("Run sample overlap simulation - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 2_simulation.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --score       Folder containing LD Scores. Should have")
    System.out.println("                  format of baseline-chr and weight-chr")
    Sysmte.out.println("    --parents     Parent information")
    Sysmte.out.println("    --siblings    Sibling information")
    Sysmte.out.println("    --sql         UK Biobank SQLite database")
    System.out.println("    --geno        Genotype file prefix ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --cov         File contain all covariates")
    System.out.println("    --drop        Samples who withdrawn consent")
    System.out.println("    --perm        Total permutation")
    System.out.println("    --effBase     Effective sample size for base. Default ${params.effBase}")
    System.out.println("    --effTarget   Effective sample size for target. Default ${params.effTarget}")
    System.out.println("    --seed        Seed for random number generation")
    System.out.println("    --runRelate   Run relatedness simulation")
    System.out.println("    --runCase     Run case simulation")
    System.out.println("    --lassosum    Path to lassosum Rscript")
    System.out.println("    --perm        Total number of permutation")
    System.out.println("    --maxPerm     Maximum number of permutation to run at the moment")
    System.out.println("    --seed        Seed for random number generation")
    System.out.println("    --help        Display this help messages")
} 

////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def get_prefix(str){
    if(str.split('-')[0] == "NA"){
        error("\n\n----------\n$str cannot be tokenized\n")
    }else 
        return str.split('-')[0];
}
def gen_file(chr, input){
    return input.replaceAll("#",chr.toString())
}

def get_chr(a, input){
    if(input.contains("#")){
        return a
    }
    else {
        return 0
    }
}
////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   removeElements
            addMeta
            removeMeta
            combine_map  }   from './modules/handle_meta'
include {   extract_valid_samples
            calculate_xbeta
            generate_phenotype
            select_base_samples
            select_target_samples }   from './modules/phenotype_simulation'
include {   run_gwas    }   from './modules/basic_genetic_analyses'
include {   run_erasor
            run_prsice
            run_lassosum
            update_prsice
            update_lassosum
            modify_erasor_meta
            combine_files
            combine_files as combine_prsice
            process_overlap_results
            combine_tmp_files  }   from './modules/polygenic_analyses'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////

genotype = Channel
    .fromFilePairs("${params.geno}.{bed,bim,fam}",size:3, flat : true){ file -> file.baseName }
    .ifEmpty { error "No matching plink files" }        
    .map { a -> [fileExists(a[1]), fileExists(a[2]), fileExists(a[3])] } 
parents = Channel.fromPath("${params.parents}")
siblings = Channel.fromPath("${params.siblings}")
qc_fam = Channel.fromPath("${params.fam}")
cov = Channel.fromPath("${params.cov}")
snp = Channel.fromPath("${params.snp}")
sql = Channel.fromPath("${params.sql}")
lassosum = Channel.fromPath("${params.lassosum}")
withdrawn = Channel.fromPath("${params.drop}")
score = Channel.fromPath("${params.score}/*") \
    | map { a -> [ get_prefix(a.baseName)+"-", file(a)]} \
    | groupTuple()

def index = 0
perm_seed = Channel
    .from( 1 .. 100000 )
    .randomSample( params.perm, params.seed )
    .map{ a-> [index++, a] }

workflow{
    phenotype_simulation()
    generate_study_samples(
        phenotype_simulation.out
    )
    generate_summary_statistics(
        generate_study_samples.out
    )
    polygenic_score_analyses(
        generate_study_samples.out,
        generate_summary_statistics.out
    )
}

workflow phenotype_simulation{
    main: 
        // 1. First, we exclude samples that doesn't pass QC or that has 
        //    withdrawn their consent to UK Biobank
        qc_fam \
            | combine(withdrawn) \
            | combine(parents) \
            | combine(siblings) \
            | extract_valid_samples
        // 2. Calculate the Xbeta given there are 10,000 causal variants
        //    Effect size were simulated with normal distribution and 
        //    we standardize the genotypes using all samples
        numCausal = Channel.of(10000)
        extract_valid_samples.out \
            | mix \
            | filter{ a -> params.runRelate || a[0].related == false} \
            | combine(numCausal) \
            | combine(perm_seed) \
            | map{ a -> def meta = a[0].clone()
                        meta["numCausal"] = a[3]
                        meta["perm"] = a[4]
                        meta["seed"] = a[5]
                        return([
                            meta,
                            a[1],
                            a[2]
                        ])} \
            | combine(genotype) \
            | combine(snp) \
            | calculate_xbeta
        heritability = Channel.of(0, 0.1, 0.5)
        prevalence = Channel.of(0.1, 0.3, 0.5)
        phenoType = Channel.of("QT", "CC")
        sharedEnv = Channel.of(0)
        xbeta = calculate_xbeta.out
        xbeta \
            | combine(heritability) \
            | combine(prevalence) \
            | combine(phenoType) \
            | combine(sharedEnv) \
            | map{ a -> def meta = a[0].clone()
                        meta["herit"] = a[3]
                        meta["type"] = a[5]
                        if(a[5] == "CC"){
                            // Need to store prevalence information for CC 
                            meta["prevalence"] = a[4]
                        }else{
                            meta["prevalence"] = 0
                        }
                        if(meta.related){
                            // only store shared environment for related samples
                            meta["shared"] = a[6]
                        }else{
                            meta["shared"] = 0
                        }
                        // now do conditional return
                        def result = [  meta,   // meta information
                                        a[1],   // related information
                                        a[2],   // beta
                                        []]     // place holder for population
                        if(meta["herit"] + meta["shared"] < 1){
                            // Avoid explaining more than 100% of the trait variance
                            if(meta.type == "CC"){
                                    // for CC, only varies prevalences when heritability is 0.1 or 0
                                    // also don't allow related samples for CC
                                    if(!meta.related && (meta.herit == 0 || meta.herit == 0.1 || meta.prevalence == 0.1)) return(result)
                            }else{
                                return(result)
                            }
                        }
            } \
            | unique \
            | filter{ a -> params.runCase || a[0].type == "QT" } \
            | generate_phenotype
            
    emit: 
        generate_phenotype.out
}


workflow generate_study_samples{
    take: pheno
    main:
        // 1. First, randomly select N samples as base. For relatedness analysis, always
        //    include most if not all samples with relatives in the base. 
        //    for related samples, we don't bother with varying the base sample size;
        //    whereas for CC, we don't want to include too much base sample as that'd
        //    be difficult to have enough samples to achieve the same effective sample size
        
        baseSize = Channel.of(250000, 120000)
        pheno \
            | combine(baseSize) \
            | map{ a -> def meta = a[0].clone()
                    meta["baseSize"] =  a[3]
                    def baseData = [    meta,   // meta information
                                        a[1],   // related information
                                        a[2],   // phenotype
                                        []]     // Stub for population
                    if(meta.baseSize != 250000 && !meta.related){
                        // only use 250000 base sample for related samples
                        if(meta.type == "QT"){
                            // only varies base size when herit is 0 or 0.1
                            if(meta.herit == 0 || meta.herit == 0.1) return(baseData)
                        }else{
                            // For CC, we've already filtered herit and prevlaence combo
                            return(baseData)
                        }
                    }else if(meta.type != "CC") return(baseData)
                } \
            | select_base_samples
        // 2. We then randomly select different numebr of target samples with various degree
        //    of overlap with base. 
        overlap = Channel.of(0, 0.05, 0.1, 0.3, 0.5, 0.6, 1)
        overlapType = Channel.of("Case", "Control", "Random")
        targetSize = Channel.of(1000, 5000, 10000, 50000)
        select_base_samples.out \
            | combine(overlap) \
            | combine(overlapType) \
            | combine(targetSize) \
            | map{ a -> def meta = a[0].clone()
                        meta["overlap"]  = a[4]
                        meta["targetSize"] = a[6]
                        if(meta.type == "CC"){
                            meta["ascertain"] = a[5]
                        }else{
                            meta["ascertain"] = "Random"
                        }
                        def result = [  meta,   // meta information
                                        a[1],   // related pair
                                        a[2],   // phenotype
                                        a[3],   // base ID
                                        []]     // stub for population
                        if(meta.related && [0, 0.3, 0.6, 1].contains(meta.overlap) && 
                            meta.baseSize == 250000 && 
                            meta.targetSize == 5000){
                            // for related samples, we only do overlap of 0, 0.3, 0.6 and 0.9
                            // only use target sample size of 5000
                            // and only use base sample size of 250000
                            // so we are iterating herit + overlap and shared environment
                            return(result)
                        }
                        else if(!meta.related && [0, 0.05, 0.1, 0.5, 1].contains(meta.overlap) ){
                            // for unrelated samples, only do overlap less than or equal to 0.5 and add in 0.9 for 
                            // QT. Difficult to do 0.9 for 
                            if(meta.type == "CC" && 
                                meta.targetSize == 5000 && 
                                meta.overlap <= 0.5 && 
                                (meta.overlap != 0 || meta.ascertain == "Random"))
                                // Only do target sample size of 5000
                                // Only differentiate different type of ascertainment when overlap != 0
                                return(result)
                            else if(meta.type == "QT" &&
                                    (meta.herit == 0 || meta.herit== 0.1 || 
                                        meta.baseSize == 250000 || 
                                        meta.targetSize == 1000))
                                // for QT, only varies the target sample size when heritability = 0 / 0.1
                                // and when base sample size is 250000
                                return(result)
                        }
                } \
            | unique \
            | select_target_samples
        // 3. Remove redundent cases, i.e. when there's no 
        //    overlap, target == expected
        samples = select_target_samples.out \
            | mix \
            | mix(select_base_samples.out)  \
            | filter{ a -> a[0].sample != "Expected" || a[0].overlap != 0 } 
    emit:
        samples
}


workflow generate_summary_statistics{
    take: samples
    main:
        // 1. Perform GWAS analysis on base and target samples
        samples \
            | filter{ a -> (a[0].perm < params.maxPerm) } \
            | filter{ a -> a[0].numCausal == 10000 || (a[0].heritability == 0.1 && meta.baseSize == 250000 &&
                                        meta.targetSize == 5000)} \
            | combine(["PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,PC11,PC12,PC13,PC14,PC15"])  \
            | map{ a ->  addMeta(x: a, meta: [cov: a[4]])} \
            | map{ a -> removeElements(x: a, idx: [4])} \
            | combine(genotype) \
            | combine(snp) \
            | combine(cov) \
            | unique \
            | run_gwas
        // 2. Extract the base GWAS
        base = run_gwas.out \
            | filter{ a -> a[0].sample == "Base"}
        target = run_gwas.out \
            | filter{ a -> a[0].sample == "Target"}
        expected = run_gwas.out \
            | filter{ a -> a[0].sample == "Expected"}
        // 3. Extract the ld score to appropriate groups
        baseline = score \
            | filter { a -> (a[0] =~ /baseline/) }
        weight = score \
            | filter { a -> (a[0] =~ /weight/) }
        // 4. Run EraSOR to obtain the adjusted GWAS
    
        combine_map(x: target, 
                    y: base, 
                    by: 0,
                    column: ["perm", "herit", "type", "prevalence", "related", "shared", "numCausal", "cov", "baseSize"]) \
            | combine(baseline) \
            | combine(weight) \
            | combine(Channel.fromPath("${params.erasor}"))\
            | run_erasor 

        gwas = run_erasor.out.gwas \
            | mix(run_erasor.out.leblanc) \
            | mix(base) \
            | mix(expected)
        
    emit: 
        gwas
}

workflow polygenic_score_analyses{
    take: samples
    take: gwas
    main:
        // 1. First, remove all base samples as we won't be using them for PRSice
        pheno = samples \
            | filter{ a -> a[0].sample == "Target"}
        baseGWAS = gwas \
            | filter{ a -> a[0].sample == "Base"}
        base = combine_map(x: baseGWAS, y: pheno, by:0, column: ["perm", "herit", "type", "prevalence", "related", "shared", "numCausal", "baseSize"])
        targetGWAS = gwas \
            | filter{ a -> a[0].sample != "Base"}
        input = combine_map(x: targetGWAS, y: pheno, by:0, column: ["perm", "herit", "type", "prevalence", "related", "shared", "numCausal", "overlap", "ascertain", "targetSize", "baseSize"]) \
            | mix(base) \
            | combine(genotype) \
            | combine(cov) \
            | unique 
        // Do not do more than 10 permutations for lassosum because of run time
        input \
            | filter{ a -> (a[0].perm < 10)} \
            | combine(lassosum) \
            | run_lassosum \
            | update_lassosum
        
        input \
            | run_prsice \
            | update_prsice \
            | mix(update_lassosum.out) \
            | groupTuple \
            | combine_tmp_files
        res = combine_tmp_files.out \
            | collect
            
        combine_prsice("prs.csv", "result", res)  \
            | combine(Channel.of("UKB.Sim")) \
            | combine(Channel.of("ukb_prs.csv")) \
            | combine(Channel.of("result")) \
            | process_overlap_results
}

