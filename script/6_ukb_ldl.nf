#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'

// Also, make sure seed is provided, otherwise we can't do -resume
timestamp='2021-12-17'
if(params.version) {
    System.out.println("")
    System.out.println("Run sample overlap simulation with GLGC LDL data- Version: $version ($timestamp)")
    exit 1
}

params.seed = 1234
params.develop = false
if(params.help){
    System.out.println("")
    System.out.println("Run sample overlap simulation - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 6_ukb_ldl.nf [options]")
    System.out.println("Mandatory arguments:")
    Sysmte.out.println("    --sql         UK Biobank SQLite database")
    System.out.println("    --score       Folder containing LD Scores. Should have")
    System.out.println("                  format of baseline-chr and weight-chr")
    System.out.println("    --geno        Genotype file prefix ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --drop        Samples who withdrawn consent")
    System.out.println("    --cov         Covariate file containing the PCs")
    System.out.println("    --ukbglgc     GLGC summary statistic with UKB data")
    System.out.println("    --glgc        GLGC summary statistic without UKB data")
    System.out.println("    --cov         Covariate file containing the PCs")
    System.out.println("    --seed        Seed for random number generation")
    System.out.println("    --help        Display this help messages")
} 



////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def get_prefix(str){
    if(str.split('-')[0] == "NA"){
        error("\n\n----------\n$str cannot be tokenized\n")
    }else 
        return str.split('-')[0];
}
def gen_file(chr, input){
    return input.replaceAll("#",chr.toString())
}

def get_chr(a, input){
    if(input.contains("#")){
        return a
    }
    else {
        return 0
    }
}
////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   removeElements
            addMeta
            removeMeta
            combine_map  }   from './modules/handle_meta'


include {   extract_valid_samples
            select_base_samples
            select_target_samples
            extract_phenotype_from_sql
            glgc_ldl_protocol  }   from './modules/phenotype_simulation'
include {   run_gwas    }   from './modules/basic_genetic_analyses'
include {   run_erasor
            run_prsice
            update_prsice
            modify_erasor_meta
            combine_files
            combine_files as combine_prsice
            combine_tmp_files
            process_real_overlap_results  }   from './modules/polygenic_analyses'

////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////
erasor = Channel.fromPath("${params.erasor}")
// usually I can write a routine to standardize the summary statistics for downstream
// analysis. But for the interest of time, and because of scale of the analysis, I have 
// premodified the summary statistics to make sure they have standardize column header
uglgc = Channel.fromPath("${params.ukbglgc}")
glgc = Channel.fromPath("${params.glgc}")
genotype = Channel
    .fromFilePairs("${params.geno}.{bed,bim,fam}",size:3, flat : true){ file -> file.baseName }
    .ifEmpty { error "No matching plink files" }        
    .map { a -> [fileExists(a[1]), fileExists(a[2]), fileExists(a[3])] } 
qc_fam = Channel.fromPath("${params.fam}")
cov = Channel.fromPath("${params.cov}")
snp = Channel.fromPath("${params.snp}")
sql = Channel.fromPath("${params.sql}")
withdrawn = Channel.fromPath("${params.drop}")

score = Channel.fromPath("${params.score}/*") \
    | map { a -> [ get_prefix(a.baseName)+"-", file(a)]} \
    | groupTuple()

phenotypes = Channel.of("30780" /*LDL*/)



workflow{
    generate_phenotype()
    
    generate_summary_statistics(
        generate_phenotype.out,
        uglgc
    )
    base = uglgc \
        | map{ a -> def meta = [:]
            meta["sample"]  = "Base"
            meta["trait"]  = "30780"
            def res = [meta, a]
            return(res)
        }
    expected = glgc \
        | map{ a -> def meta = [:]
            meta["sample"]  = "Expected"
            meta["trait"]  = "30780"
            def res = [meta, a]
            return(res)
        }
    gwas = base \
        | mix(expected) \
        | mix(generate_summary_statistics.out) 
    // now we want to join and mock the sumstat so that they
    // follow the expected format, allowing us to reuse the 
    // downstream 
    
    polygenic_score_analyses(
        generate_phenotype.out,
        gwas
    )
}


workflow generate_phenotype(){
    phenotypes \
        | combine(sql) \
        | extract_phenotype_from_sql \
        | combine(withdrawn) \
        | combine(qc_fam) \
        | combine(cov) \
        | glgc_ldl_protocol
    pheno = glgc_ldl_protocol.out.sampleSize \
        | splitCsv(header: true) \
        | map{ a -> [a.FieldID, a.SampleSize] } \
        | combine(glgc_ldl_protocol.out.pheno, by: 0)
    emit: 
    pheno = pheno
}


workflow generate_summary_statistics{
    take: samples
    take: glgc
    main:
        // 1. As we skipped sample generation, the data will now only
        //    contain "Target" sample, and won't have any related samples
        samples \
            | map{ a -> 
                def meta  = [:]
                meta["trait"] = a[0]
                meta["sample"] = "Target"
                def res = [ meta, 
                            [],     // we don't have related samples
                            a[2],   // phenotype
                            []      // selected sample (all)
                            ]
                return(res)
            } \
            | combine(["NA"])  \
            | map{ a ->  addMeta(x: a, meta: [cov: a[4]])} \
            | map{ a -> removeElements(x: a, idx: [4])} \
            | combine(genotype) \
            | combine(snp) \
            | combine(cov) \
            | unique \
            | run_gwas
        // 2. Extract the base GWAS
        target = run_gwas.out \
            | filter{ a -> a[0].sample == "Target"}
        // 3. Extract the ld score to appropriate groups
        baseline = score \
            | filter { a -> (a[0] =~ /baseline/) }
        weight = score \
            | filter { a -> (a[0] =~ /weight/) }
        // 4. Run EraSOR to obtain the adjusted GWAS
        target \
            | combine(glgc) \
            | combine(baseline) \
            | combine(weight) \
            | combine(erasor) \
            | run_erasor
            
    emit: 
        run_erasor.out.gwas
}


workflow polygenic_score_analyses{
    take: samples
    take: gwas
    main:
    // we will need to run three different PRSice analysis
    // 1. With UKB 
    // 2. Without UKB
    // 3. With UKB but use EraSOR to adjust
        // 1. First, remove all base samples as we won't be using them for PRSice
        baseGWAS = gwas \
            | filter{ a -> a[0].sample == "Base"}
        pheno = samples \
            | map{ a -> def meta = [:]
            meta["trait"] = a[0]
            // add an empty related and sample file
            def res = [meta, [], a[2], []]
            return(res)}
        
        base = combine_map(x: baseGWAS, y: pheno, by:0, column: ["trait"])
        base \
            | view
        targetGWAS = gwas \
            | filter{ a -> a[0].sample != "Base"}
        combine_map(x: targetGWAS, y: pheno, by:0, column: [ "trait"]) \
            | view \
            | mix(base) \
            | combine(genotype) \
            | combine(cov) \
            | unique \
            | run_prsice \
            | map{ a -> def meta = a[0].clone()
                // we will use Ascertainment to store our trait name, as such we can reuse the update_prsice process
                meta["ascertain"]  = meta.trait
                return [meta,
                        a[1]]} \
            | update_prsice \
            | groupTuple \
            | combine_tmp_files
        res = combine_tmp_files.out \
            | collect
        combine_prsice("real_prs.csv", "result", res) \
            | combine(Channel.of("UKB.Real")) \
            | combine(Channel.of("ukb-real-prs.csv")) \
            | combine(Channel.of("result")) \
            | process_real_overlap_results
            
}

